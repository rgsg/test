-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 47.236.99.16    Database: test
-- ------------------------------------------------------
-- Server version	5.6.51-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `area_delivery`
--

DROP TABLE IF EXISTS `area_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `area_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) NOT NULL COMMENT '区域id',
  `delivery_id` int(11) NOT NULL COMMENT '配送员id',
  PRIMARY KEY (`id`),
  KEY `area_delivery_idx` (`area_id`,`delivery_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='区域配送信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area_delivery`
--

LOCK TABLES `area_delivery` WRITE;
/*!40000 ALTER TABLE `area_delivery` DISABLE KEYS */;
/*!40000 ALTER TABLE `area_delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_area`
--

DROP TABLE IF EXISTS `p_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `p_area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '区域id',
  `area_code` varchar(32) DEFAULT NULL COMMENT '区域编号',
  `name` varchar(100) DEFAULT NULL COMMENT '区域名称',
  `area_color` varchar(32) DEFAULT NULL COMMENT '区域颜色',
  `border_color` varchar(32) DEFAULT NULL COMMENT '边框颜色',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户id',
  `org_code` varchar(32) DEFAULT NULL COMMENT '机构码',
  `parent_id` int(11) DEFAULT NULL COMMENT '父区域',
  `type_code` varchar(32) DEFAULT NULL COMMENT '区域类型编码',
  `scope` varchar(100) DEFAULT NULL COMMENT '区域范围',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `modify_by` varchar(32) DEFAULT NULL COMMENT '最后修改人',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '最后修改日期',
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=286 DEFAULT CHARSET=utf8mb4 COMMENT='区域表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_area`
--

LOCK TABLES `p_area` WRITE;
/*!40000 ALTER TABLE `p_area` DISABLE KEYS */;
INSERT INTO `p_area` VALUES (1,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-30 14:02:37',NULL,NULL,'2024-04-29 14:02:37'),(7,NULL,'111',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(8,NULL,'test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(9,NULL,'test2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(10,NULL,'test3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(11,NULL,'test4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(12,NULL,'test5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(13,NULL,'test6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(14,NULL,'test7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(15,NULL,'test8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(16,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:11:34',NULL,NULL,'2024-04-28 10:11:34'),(17,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:12:17',NULL,NULL,'2024-04-28 10:12:17'),(18,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:12:46',NULL,NULL,'2024-04-28 10:12:46'),(19,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:13:35',NULL,NULL,'2024-04-28 10:13:35'),(20,NULL,'111',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(21,NULL,'test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(22,NULL,'test2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(23,NULL,'test3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(24,NULL,'test4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(25,NULL,'test5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(26,NULL,'test6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(27,NULL,'test7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(28,NULL,'test8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(29,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:14:16',NULL,NULL,'2024-04-28 10:14:16'),(30,NULL,'111',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(31,NULL,'test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(32,NULL,'test2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(33,NULL,'test3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(34,NULL,'test4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(35,NULL,'test5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(36,NULL,'test6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(37,NULL,'test7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(38,NULL,'test8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(39,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:20:04',NULL,NULL,'2024-04-28 10:20:04'),(40,NULL,'111',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(41,NULL,'test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(42,NULL,'test2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(43,NULL,'test3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(44,NULL,'test4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(45,NULL,'test5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(46,NULL,'test6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(47,NULL,'test7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(48,NULL,'test8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(49,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:24:45',NULL,NULL,'2024-04-28 10:24:45'),(50,NULL,'111',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(51,NULL,'test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(52,NULL,'test2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(53,NULL,'test3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(54,NULL,'test4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(55,NULL,'test5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(56,NULL,'test6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(57,NULL,'test7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(58,NULL,'test8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(59,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:28:46',NULL,NULL,'2024-04-28 10:28:46'),(60,NULL,'111',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(61,NULL,'test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(62,NULL,'test2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(63,NULL,'test3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(64,NULL,'test4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(65,NULL,'test5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(66,NULL,'test6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(67,NULL,'test7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(68,NULL,'test8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(69,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:31:39',NULL,NULL,'2024-04-28 10:31:39'),(70,NULL,'111',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(71,NULL,'test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(72,NULL,'test2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(73,NULL,'test3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(74,NULL,'test4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(75,NULL,'test5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(76,NULL,'test6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(77,NULL,'test7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(78,NULL,'test8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(79,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:01',NULL,NULL,'2024-04-28 10:32:01'),(80,NULL,'111',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(81,NULL,'test1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(82,NULL,'test2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(83,NULL,'test3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(84,NULL,'test4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(85,NULL,'test5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(86,NULL,'test6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(87,NULL,'test7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(88,NULL,'test8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(89,NULL,'test9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 10:32:15',NULL,NULL,'2024-04-28 10:32:15'),(100,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:06:39',NULL,NULL,'2024-04-28 15:06:39'),(101,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:06:39',NULL,NULL,'2024-04-28 15:06:39'),(102,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:07:19',NULL,NULL,'2024-04-28 15:07:19'),(103,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:07:19',NULL,NULL,'2024-04-28 15:07:19'),(104,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:07:30',NULL,NULL,'2024-04-28 15:07:30'),(105,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:07:30',NULL,NULL,'2024-04-28 15:07:30'),(106,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:08:25',NULL,NULL,'2024-04-28 15:08:25'),(107,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:08:25',NULL,NULL,'2024-04-28 15:08:25'),(108,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:11:10',NULL,NULL,'2024-04-28 15:11:10'),(109,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:11:10',NULL,NULL,'2024-04-28 15:11:10'),(110,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:13:30',NULL,NULL,'2024-04-28 15:13:30'),(111,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:13:30',NULL,NULL,'2024-04-28 15:13:30'),(112,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:14:36',NULL,NULL,'2024-04-28 15:14:36'),(113,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:14:36',NULL,NULL,'2024-04-28 15:14:36'),(114,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:34:37',NULL,NULL,'2024-04-28 15:34:37'),(115,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:34:37',NULL,NULL,'2024-04-28 15:34:37'),(116,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:38:05',NULL,NULL,'2024-04-28 15:38:05'),(117,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 15:38:05',NULL,NULL,'2024-04-28 15:38:05'),(118,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:03:10',NULL,NULL,'2024-04-28 16:03:10'),(119,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:04:03',NULL,NULL,'2024-04-28 16:04:03'),(120,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:05:15',NULL,NULL,'2024-04-28 16:05:15'),(121,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:05:52',NULL,NULL,'2024-04-28 16:05:52'),(122,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:07:27',NULL,NULL,'2024-04-28 16:07:27'),(123,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:09:12',NULL,NULL,'2024-04-28 16:09:12'),(124,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:09:27',NULL,NULL,'2024-04-28 16:09:27'),(125,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:09:37',NULL,NULL,'2024-04-28 16:09:37'),(126,'001','??1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:10:57',NULL,NULL,'2024-04-28 16:10:57'),(127,'002','??2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:10:57',NULL,NULL,'2024-04-28 16:10:57'),(128,'22','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:53:40',NULL,NULL,'2024-04-28 16:53:40'),(129,'22','11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-28 16:55:37',NULL,NULL,'2024-04-28 16:55:37'),(130,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 09:48:54',NULL,NULL,'2024-04-29 09:48:54'),(131,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 09:54:22',NULL,NULL,'2024-04-29 09:54:22'),(132,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 09:58:36',NULL,NULL,'2024-04-29 09:58:36'),(133,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 10:11:21',NULL,NULL,'2024-04-29 10:11:21'),(134,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 10:55:57',NULL,NULL,'2024-04-29 10:55:57'),(135,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:14:07',NULL,NULL,'2024-04-29 11:14:07'),(136,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:14:07',NULL,NULL,'2024-04-29 11:14:07'),(137,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:23:08',NULL,NULL,'2024-04-29 11:23:08'),(138,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:24:50',NULL,NULL,'2024-04-29 11:24:50'),(139,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:25:42',NULL,NULL,'2024-04-29 11:25:42'),(140,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:28:27',NULL,NULL,'2024-04-29 11:28:27'),(141,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:30:42',NULL,NULL,'2024-04-29 11:30:42'),(142,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:33:28',NULL,NULL,'2024-04-29 11:33:28'),(143,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:39:11',NULL,NULL,'2024-04-29 11:39:11'),(144,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:44:41',NULL,NULL,'2024-04-29 11:44:41'),(145,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 11:50:52',NULL,NULL,'2024-04-29 11:50:52'),(146,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:25:25',NULL,NULL,'2024-04-29 13:25:25'),(147,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:27:14',NULL,NULL,'2024-04-29 13:27:14'),(148,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:29:19',NULL,NULL,'2024-04-29 13:29:19'),(149,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:31:54',NULL,NULL,'2024-04-29 13:31:54'),(150,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:32:50',NULL,NULL,'2024-04-29 13:32:50'),(151,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:36:37',NULL,NULL,'2024-04-29 13:36:37'),(152,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:37:14',NULL,NULL,'2024-04-29 13:37:14'),(153,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:37:31',NULL,NULL,'2024-04-29 13:37:31'),(154,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:39:39',NULL,NULL,'2024-04-29 13:39:39'),(155,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:41:03',NULL,NULL,'2024-04-29 13:41:03'),(156,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:43:53',NULL,NULL,'2024-04-29 13:43:53'),(157,'22','44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:46:23',NULL,NULL,'2024-04-29 13:46:23'),(158,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:55:06',NULL,NULL,'2024-04-29 13:55:06'),(159,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:55:06',NULL,NULL,'2024-04-29 13:55:06'),(160,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:58:32',NULL,NULL,'2024-04-29 13:58:32'),(161,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 13:58:32',NULL,NULL,'2024-04-29 13:58:32'),(188,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 14:02:37',NULL,NULL,'2024-04-29 14:02:37'),(189,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 14:16:47',NULL,NULL,'2024-04-29 14:16:47'),(190,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-04-29 14:16:47',NULL,NULL,'2024-04-29 14:16:47'),(191,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 14:13:28',NULL,NULL,'2024-05-07 14:13:28'),(192,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 14:13:28',NULL,NULL,'2024-05-07 14:13:28'),(193,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 14:14:30',NULL,NULL,'2024-05-07 14:14:30'),(194,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 14:14:30',NULL,NULL,'2024-05-07 14:14:30'),(195,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 14:30:40',NULL,NULL,'2024-05-07 14:30:40'),(196,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 14:30:40',NULL,NULL,'2024-05-07 14:30:40'),(197,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 15:14:56',NULL,NULL,'2024-05-07 15:14:56'),(198,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 15:14:56',NULL,NULL,'2024-05-07 15:14:56'),(199,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 15:33:59',NULL,NULL,'2024-05-07 15:33:59'),(200,'33','33',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-07 15:33:59',NULL,NULL,'2024-05-07 15:33:59'),(203,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:00:02',NULL,NULL,'2024-05-08 15:00:02'),(204,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:22:48',NULL,NULL,'2024-05-08 15:22:48'),(205,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:23:36',NULL,NULL,'2024-05-08 15:23:36'),(206,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:28:56',NULL,NULL,'2024-05-08 15:28:56'),(207,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:30:44',NULL,NULL,'2024-05-08 15:30:44'),(208,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:34:15',NULL,NULL,'2024-05-08 15:34:15'),(209,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:43:34',NULL,NULL,'2024-05-08 15:43:34'),(210,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:49:09',NULL,NULL,'2024-05-08 15:49:09'),(211,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:49:23',NULL,NULL,'2024-05-08 15:49:23'),(212,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 15:51:07',NULL,NULL,'2024-05-08 15:51:07'),(213,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 16:07:45',NULL,NULL,'2024-05-08 16:07:45'),(214,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 16:29:18',NULL,NULL,'2024-05-08 16:29:18'),(215,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 16:29:21',NULL,NULL,'2024-05-08 16:29:21'),(216,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 16:29:25',NULL,NULL,'2024-05-08 16:29:25'),(217,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 16:31:44',NULL,NULL,'2024-05-08 16:31:44'),(218,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 16:37:58',NULL,NULL,'2024-05-08 16:37:58'),(219,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 16:40:21',NULL,NULL,'2024-05-08 16:40:21'),(220,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 16:43:40',NULL,NULL,'2024-05-08 16:43:40'),(221,'22','22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-08 17:28:51',NULL,NULL,'2024-05-08 17:28:51'),(226,'ceshi','ceshi',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-14 09:45:30',NULL,NULL,'2024-05-14 09:45:30'),(234,'ceshi','测试啊',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-14 11:13:48',NULL,NULL,'2024-05-14 11:13:46'),(235,'ceshi','测试啊',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-14 11:15:40',NULL,NULL,'2024-05-14 11:15:38'),(236,'ceshi','测试啊',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-14 11:17:19',NULL,NULL,'2024-05-14 11:17:02'),(237,'ceshi','测试啊',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-14 11:31:22',NULL,NULL,'2024-05-14 11:31:20'),(238,'ceshi','测试啊',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-14 11:35:03',NULL,NULL,'2024-05-14 11:35:00'),(239,'ceshi','测试啊',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-05-14 11:40:34',NULL,NULL,'2024-05-14 11:40:32'),(278,'aaa','aaa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-10-11 09:38:12',NULL,NULL,'2024-10-10 20:38:10'),(279,'aaa','aaa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-10-11 10:49:22',NULL,NULL,'2024-10-10 21:36:59'),(280,'aaa','aaa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-10-11 11:02:02',NULL,NULL,'2024-10-10 22:02:01'),(281,'aaa','aaa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2024-10-11 11:04:20',NULL,NULL,'2024-10-10 22:04:19');
/*!40000 ALTER TABLE `p_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_car_info`
--

DROP TABLE IF EXISTS `p_car_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `p_car_info` (
  `id` int(11) DEFAULT NULL,
  `license_no` varchar(20) NOT NULL COMMENT '车牌号',
  `is_gps` tinyint(2) DEFAULT '0' COMMENT 'gps是否安装 1是',
  `gps_state` tinyint(2) DEFAULT NULL COMMENT 'gps状态',
  `driver_no` varchar(32) DEFAULT NULL COMMENT '司机工号',
  `supercargo_no` varchar(32) DEFAULT NULL COMMENT '押运员工号',
  `state` varchar(32) DEFAULT NULL COMMENT '车辆状态',
  `type_code` varchar(32) DEFAULT NULL COMMENT '类型编码',
  `model_code` varchar(32) DEFAULT NULL COMMENT '型号编码',
  `brand_code` varchar(32) DEFAULT NULL COMMENT '品牌编码',
  `use_date` datetime DEFAULT NULL COMMENT '使用日期',
  `org_code` varchar(32) NOT NULL COMMENT '机构码',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户id',
  `license` varchar(100) DEFAULT NULL COMMENT '道路运输许可证号',
  `mark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `modify_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  UNIQUE KEY `license_no_IDX` (`license_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='车辆信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_car_info`
--

LOCK TABLES `p_car_info` WRITE;
/*!40000 ALTER TABLE `p_car_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_car_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_delivery_man`
--

DROP TABLE IF EXISTS `p_delivery_man`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `p_delivery_man` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_no` varchar(50) NOT NULL COMMENT '员工编号',
  `tenant_id` varchar(32) NOT NULL COMMENT '租户id',
  `org_code` varchar(32) DEFAULT NULL COMMENT '机构码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(32) DEFAULT NULL COMMENT '创建人',
  `modify_by` varchar(50) DEFAULT NULL COMMENT '修改人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='配送员表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_delivery_man`
--

LOCK TABLES `p_delivery_man` WRITE;
/*!40000 ALTER TABLE `p_delivery_man` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_delivery_man` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `p_station`
--

DROP TABLE IF EXISTS `p_station`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `p_station` (
  `station_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '站点id',
  `area_id` int(11) DEFAULT NULL COMMENT '所属区域',
  `station_code` varchar(32) DEFAULT NULL COMMENT '站点编号',
  `station_name` varchar(32) NOT NULL COMMENT '站点名称',
  `gps` varchar(100) DEFAULT NULL COMMENT '坐标',
  `type_code` varchar(32) DEFAULT NULL COMMENT '类型编码',
  `type_name` varchar(32) DEFAULT NULL COMMENT '类型名称',
  `status_code` varchar(32) DEFAULT NULL COMMENT '状态编码',
  `status_name` varchar(32) DEFAULT NULL COMMENT '状态名称',
  `property_code` varchar(32) DEFAULT NULL COMMENT '性质编码',
  `property_name` varchar(50) DEFAULT NULL COMMENT '性质名称',
  `org_code` varchar(32) DEFAULT NULL COMMENT '机构码',
  `tenant_id` varchar(32) DEFAULT NULL COMMENT '租户id',
  `province_code` varchar(30) DEFAULT NULL COMMENT '省份编码',
  `city_code` varchar(30) DEFAULT NULL COMMENT '市级编码',
  `zone_code` varchar(30) DEFAULT NULL COMMENT '区级编码',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `create_by` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_by` int(11) DEFAULT NULL COMMENT '最后修改人',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '最后修改日期',
  PRIMARY KEY (`station_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `p_station`
--

LOCK TABLES `p_station` WRITE;
/*!40000 ALTER TABLE `p_station` DISABLE KEYS */;
/*!40000 ALTER TABLE `p_station` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_dict`
--

DROP TABLE IF EXISTS `s_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `s_dict` (
  `dict_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '数据字典id',
  `sort_name` varchar(32) DEFAULT NULL COMMENT '分类名称',
  `sort_code` varchar(32) DEFAULT NULL COMMENT '分类编码',
  `name` varchar(32) DEFAULT NULL COMMENT '字典参数名称',
  `default_value` varchar(64) DEFAULT NULL COMMENT '字典参数值',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态1有效0无效',
  `sort_no` tinyint(3) unsigned DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`dict_id`),
  KEY `ix_sort_code` (`sort_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='数据字典表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_dict`
--

LOCK TABLES `s_dict` WRITE;
/*!40000 ALTER TABLE `s_dict` DISABLE KEYS */;
INSERT INTO `s_dict` VALUES (1,'车辆管理','10001','车辆状态','10010001',1,1,'2024-03-11 17:18:48','2024-03-11 17:18:48'),(2,'车辆管理','10001','车辆品牌','10010002',1,2,'2024-03-11 17:19:34','2024-03-11 17:19:34'),(3,'站点管理','10002','站点性质','10020001',1,1,'2024-03-11 17:19:56','2024-03-11 17:19:56'),(11,NULL,NULL,'哈哈测试',NULL,NULL,NULL,'2024-09-06 15:09:06','2024-09-06 15:09:06'),(12,NULL,NULL,'哈哈测试',NULL,NULL,NULL,'2024-09-06 15:28:03','2024-09-06 15:28:03');
/*!40000 ALTER TABLE `s_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `Column8` varchar(100) NOT NULL,
  `id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-10-11 16:22:16
