package org.example.filter;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;

import java.util.Map;

/**
 * @author cai ping
 * @description:
 * @date 2024-11-07
 */
public class SampleFilter extends Filter<ILoggingEvent> {
    @Override
    public FilterReply decide(ILoggingEvent event) {
        Map<String, String> mdcPropertyMap = event.getMDCPropertyMap();
        if(mdcPropertyMap.containsKey("requestId")) {
            return FilterReply.ACCEPT;
        }
        return FilterReply.DENY;
    }
}
