package org.example.kafka.product;

import ch.qos.logback.classic.util.ContextInitializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.example.kafka.interceptors.TraceInterceptor;

import java.util.HashMap;

/**
 * @author cai ping
 * @description:
 * @date 2024-11-11
 */
@Slf4j
public class TestProduct {

    public static void main(String[] args) {

        System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY, "D:\\code\\my\\demo\\src\\main\\resources\\logback-spring.xml");

        HashMap<String, Object> stringMap = new HashMap<>();
        stringMap.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "120.26.70.222:31092");
        stringMap.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        stringMap.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        stringMap.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, TraceInterceptor.class.getName());

        try (KafkaProducer<String, Object> kafkaProducer = new KafkaProducer<>(stringMap)) {

            ProducerRecord<String, Object> record = new ProducerRecord<>("test-topic", null, "value1");

            kafkaProducer.send(record, (metadata, exception) -> {
                if (metadata == null) {
                    log.error("消息发送失败,exception{}", exception);
                } else {
                    log.info("消息发送成功,metadata={}", metadata);
                }
            });

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
