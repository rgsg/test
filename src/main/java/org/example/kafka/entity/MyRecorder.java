package org.example.kafka.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author cai ping
 * @description:
 * @date 2024-11-11
 */
@Data
public class MyRecorder implements Serializable {

    private Object data;

    private String traceId;

}
