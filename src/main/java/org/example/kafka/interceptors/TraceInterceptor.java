package org.example.kafka.interceptors;

import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.example.kafka.entity.MyRecorder;

import java.util.Map;
import java.util.UUID;

/**
 * @author cai ping
 * @description:
 * @date 2024-11-11
 */
public class TraceInterceptor<K,V extends String> implements ProducerInterceptor<K, String> {


    @Override
    public ProducerRecord<K, String> onSend(ProducerRecord<K, String> record) {
        MyRecorder myRecorder = new MyRecorder();
        myRecorder.setTraceId(UUID.randomUUID().toString());
        myRecorder.setData(record.value());
        return new ProducerRecord<>(record.topic(), record.partition(), record.key(), JSONObject.toJSONString(myRecorder));
    }

    @Override
    public void onAcknowledgement(RecordMetadata metadata, Exception exception) {

    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<java.lang.String, ?> configs) {

    }
}
