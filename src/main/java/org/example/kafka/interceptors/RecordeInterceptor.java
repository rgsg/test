package org.example.kafka.interceptors;

import cn.hutool.json.JSONUtil;
import org.apache.kafka.clients.consumer.ConsumerInterceptor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cai ping
 * @description:
 * @date 2024-11-11
 */
public class RecordeInterceptor implements ConsumerInterceptor<String, String> {
    @Override
    public ConsumerRecords<String, String> onConsume(ConsumerRecords<String, String> records) {

        Map<TopicPartition, List<ConsumerRecord<String, String>>> recordMap = new HashMap<>();
        ConsumerRecords<String, String> consumerRecords = new ConsumerRecords<>(recordMap);

        for (ConsumerRecord<String, String> record : records) {
            String value = record.value();
            String key = record.key();
            int partition = record.partition();
            long offset = record.offset();
            String topic = record.topic();
            String data = (String) JSONUtil.parseObj(value).get("data");
            ConsumerRecord<String, String> consumerRecord = new ConsumerRecord<>(topic, partition, offset, key, data);

            TopicPartition topicPartition = new TopicPartition(topic, partition);
            List<ConsumerRecord<String, String>> recordList = recordMap.getOrDefault(topicPartition, new ArrayList<>());
            recordList.add(consumerRecord);
            recordMap.putIfAbsent(topicPartition,recordList);

        }

        return consumerRecords;
    }

    public static void main(String[] args) {

        TopicPartition topicPartition1 = new TopicPartition("topic", 0);

        TopicPartition topicPartition2 = new TopicPartition("topic", 0);

        System.out.println(topicPartition1.equals(topicPartition2));
    }

    @Override
    public void onCommit(Map<TopicPartition, OffsetAndMetadata> offsets) {

    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> configs) {

    }
}
