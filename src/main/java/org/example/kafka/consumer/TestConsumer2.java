package org.example.kafka.consumer;

import ch.qos.logback.classic.util.ContextInitializer;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashMap;

/**
 * @author cai ping
 * @description:
 * @date 2024-11-11
 */
public class TestConsumer2 {

    /**
     * 分区分配策略:
     *  1. 顺序分区
     *  2. range分区
     *  3. 粘性分区
     */

    public static void main(String[] args) {

        System.setProperty(ContextInitializer.CONFIG_FILE_PROPERTY,"D:\\code\\my\\demo\\src\\main\\resources\\logback-spring.xml");


        HashMap<String, Object> stringMap = new HashMap<>();
        stringMap.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"120.26.70.222:31092");
        stringMap.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        stringMap.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class);
        stringMap.put(ConsumerConfig.GROUP_ID_CONFIG,"test-03");
        stringMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"earliest");

        stringMap.put(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, StickyAssignor.class.getName());
        stringMap.put(ConsumerConfig.CLIENT_ID_CONFIG, "bbb");

        try(KafkaConsumer<String,String> kafkaConsumer =  new KafkaConsumer<>(stringMap)) {

            kafkaConsumer.subscribe(Arrays.asList("test-topic"));

            while (true) {
                ConsumerRecords<String, String> records = kafkaConsumer.poll(Duration.of(2, ChronoUnit.SECONDS));
                for (ConsumerRecord<String,String> record : records) {
                    System.out.println("record = " + record);
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}
