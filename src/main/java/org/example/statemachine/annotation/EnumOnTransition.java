package org.example.statemachine.annotation;


import org.example.statemachine.enums.OrderStates;
import org.springframework.statemachine.annotation.OnTransition;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@OnTransition
public @interface EnumOnTransition {

    OrderStates[] source() default {};

    OrderStates[] target() default {};
}
