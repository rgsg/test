package org.example.statemachine;

import lombok.extern.slf4j.Slf4j;
import org.example.mapper.mysql1.OrderMapper;
import org.example.statemachine.annotation.EnumOnTransition;
import org.example.statemachine.entity.Order;
import org.example.statemachine.enums.OrderStates;
import org.springframework.messaging.Message;
import org.springframework.statemachine.annotation.WithStateMachine;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

//绑定待监听的状态机
@Component
@WithStateMachine
@Slf4j
public class OrderEventListener {



    @EnumOnTransition(target = OrderStates.UNPAID)
    public boolean create(Message<String> message) {
//        Order order = (Order) message.getHeaders().get("order");
        log.info("订单创建，待支付!");
        log.info("message = {}", message);
//        if(Objects.nonNull(order)) {
//            order.setOrderStates(OrderStates.UNPAID);
        return true;
//        }else {
//            return false;
//        }
    }
    @Resource
    private OrderMapper orderMapper;

    @EnumOnTransition(source = OrderStates.UNPAID,target = OrderStates.WAITING_FOR_DELIVER)
    public boolean pay(Message<String> message) {
        Order order = (Order) message.getHeaders().get("order");
        log.info("{}用户完成支付，待发货!", order);
        log.info("message = {}", message);
        if (Objects.nonNull(order)) {
            order.setOrderStates(OrderStates.WAITING_FOR_DELIVER);
            orderMapper.updateById(order);
            return true;
        } else {
            return false;
        }
    }

    @EnumOnTransition(source = OrderStates.WAITING_FOR_DELIVER, target = OrderStates.WAITING_FOR_RECEIVE)
    public boolean deliver(Message<String> message) {
        Order order = (Order) message.getHeaders().get("order");
        log.info("订单已发货，待收货!");
        log.info("message = {}", message);
        if (Objects.nonNull(order)) {
            order.setOrderStates(OrderStates.WAITING_FOR_RECEIVE);
            orderMapper.updateById(order);
            return true;
        } else {
            return false;
        }
    }

    @EnumOnTransition(source = OrderStates.WAITING_FOR_RECEIVE,target = OrderStates.DONE)
    public boolean receive(Message<String> message) {
        Order order = (Order) message.getHeaders().get("order");
        log.info("用户已收货，订单完成!");
        log.info("message = {}", message);
        if (Objects.nonNull(order)) {
            order.setOrderStates(OrderStates.DONE);
            orderMapper.updateById(order);
            return true;
        } else {
            return false;
        }
    }
}
