package org.example.statemachine;

import lombok.extern.slf4j.Slf4j;
import org.example.statemachine.entity.Order;
import org.example.statemachine.enums.OrderEvents;
import org.example.statemachine.enums.OrderStates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.support.DefaultStateMachineContext;

import java.util.EnumSet;

@Configuration
@EnableStateMachine            //启用状态机
@Slf4j
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<OrderStates, OrderEvents> {

    @Override
    public void configure(StateMachineConfigurationConfigurer<OrderStates, OrderEvents> config)
            throws Exception {
        config
                .withConfiguration()
                .autoStartup(true);
    }

    //配置初始状态
    @Override
    public void configure(StateMachineStateConfigurer<OrderStates, OrderEvents> states)
            throws Exception {
        states
                .withStates()
                .initial(OrderStates.UNPAID)
                .states(EnumSet.allOf(OrderStates.class));
    }

    //配置状态转换的事件关系（多个）
    @Override
    public void configure(StateMachineTransitionConfigurer<OrderStates, OrderEvents> transitions)
            throws Exception {
        transitions
                .withExternal()
                .source(OrderStates.UNPAID).target(OrderStates.WAITING_FOR_DELIVER).event(OrderEvents.PAY)

                .and()
                .withExternal()
                .source(OrderStates.WAITING_FOR_DELIVER).target(OrderStates.WAITING_FOR_RECEIVE)
                .event(OrderEvents.DELIVER)
                .and()
                .withExternal()
                .source(OrderStates.WAITING_FOR_RECEIVE).target(OrderStates.DONE).event(OrderEvents.RECEIVE);
    }

    @Bean(name = "stateMachineMemPersister")
    public DefaultStateMachinePersister<OrderStates,OrderEvents,Order> persister() {
        return new DefaultStateMachinePersister<>(new StateMachinePersist<OrderStates, OrderEvents, Order>() {

            @Override
            public void write(StateMachineContext<OrderStates, OrderEvents> stateMachineContext, Order order) {
                log.info("--------------persister-write:{}", order);
            }

            @Override
            public StateMachineContext<OrderStates, OrderEvents> read(Order order) {
                log.info("read order = {}",order);
                return new DefaultStateMachineContext<>(order.getOrderStates(), null, null, null);
            }
        });
    }




}
