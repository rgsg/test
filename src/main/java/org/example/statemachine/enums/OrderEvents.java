package org.example.statemachine.enums;

public enum OrderEvents {
        PAY,        // 支付
        DELIVER,    // 发货
        RECEIVE     // 收货
}
