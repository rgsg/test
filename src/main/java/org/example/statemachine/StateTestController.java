package org.example.statemachine;

import lombok.extern.slf4j.Slf4j;
import org.example.mapper.mysql1.OrderMapper;
import org.example.statemachine.entity.Order;
import org.example.statemachine.enums.OrderEvents;
import org.example.statemachine.enums.OrderStates;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.persist.StateMachinePersister;
import org.springframework.statemachine.support.AbstractStateMachine;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author cai ping
 * @description:
 * @date 2024-12-12
 */
@RestController
@RequestMapping("/state")
@Slf4j
public class StateTestController {

    @Resource
    private OrderMapper orderMapper;

    @Resource
    AbstractStateMachine<OrderStates, OrderEvents> stateMachine;


    // 状态机测试
    @GetMapping("/create")
    public void create(String orderId) {
//        Order order = Order.builder().id(orderId).orderStates(OrderStates.UNPAID).orderNo(orderId).build();
//        orderMapper.insert(order);
    }

    @PostMapping("/createOrder")
    public void create(@RequestBody Order order) {
        orderMapper.insert(order);
    }

    @GetMapping("/pay")
    public void pay(String orderId) {

        boolean flag = getOrderMessage(orderId, OrderEvents.PAY);
        if (flag) {
            log.info("pay success!");
        } else {
            log.info("pay error!");
        }
    }

    @GetMapping("/deliver")
    public void deliver(String orderId) {
        boolean flag = getOrderMessage(orderId, OrderEvents.DELIVER);
        if (flag) {
            log.info("deliver success!");
        } else {
            log.info("deliver error!");
        }
    }

    @GetMapping("/receive")
    public void receive(String orderId) {
        boolean flag = getOrderMessage(orderId, OrderEvents.RECEIVE);
        if (flag) {
            log.info("receive success!");
        } else {
            log.info("receive error!");
        }
    }

    @Resource
    private StateMachinePersister<OrderStates, OrderEvents, Order> persister;


    private synchronized boolean getOrderMessage(String orderId, OrderEvents events) {
        boolean flag;
        try {
            Order order = orderMapper.selectById(orderId);

            if (Objects.isNull(order)) {
                log.error("订单不存在!!!");
                return false;
            }
//            stateMachine.start();
//            persister.restore(stateMachine, order);

            stateMachine.resetStateMachine(new DefaultStateMachineContext<>(order.getOrderStates(), null, null, null));
            flag = stateMachine.sendEvent(MessageBuilder.withPayload(events).setHeader("order", order).build());
            persister.persist(stateMachine, order);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (stateMachine != null) {
//                stateMachine.stop();
            }
        }
        return flag;
    }

}
