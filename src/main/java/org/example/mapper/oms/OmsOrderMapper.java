package org.example.mapper.oms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.entity.OmsOrder;

@Mapper
public interface OmsOrderMapper extends BaseMapper<OmsOrder> {
    int test();
}
