package org.example.mapper.oms;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.entity.Category;

@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
