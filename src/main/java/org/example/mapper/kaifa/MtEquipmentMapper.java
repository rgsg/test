package org.example.mapper.kaifa;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.entity.MtEquipment;

@Mapper
//@DS("kaifa")
public interface MtEquipmentMapper extends BaseMapper<MtEquipment> {
    /**
     * delete by primary key
     * @param id primaryKey
     * @return deleteCount
     */
//    int deleteByPrimaryKey(Integer id);

    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
//    int insert(MtEquipment record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
//    int insertSelective(MtEquipment record);

    /**
     * select by primary key
     * @param id primary key
     * @return object by primary key
     */
//    MtEquipment selectByPrimaryKey(Integer id);

    /**
     * update record selective
     * @param record the updated record
     * @return update count
     */
//    int updateByPrimaryKeySelective(MtEquipment record);

    /**
     * update record
     * @param record the updated record
     * @return update count
     */
//    int updateByPrimaryKey(MtEquipment record);
}