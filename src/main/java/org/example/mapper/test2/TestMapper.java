package org.example.mapper.test2;


//import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.entity.Test;

//@DS("slave")
public interface TestMapper extends BaseMapper<Test> {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(Test record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(Test record);
}