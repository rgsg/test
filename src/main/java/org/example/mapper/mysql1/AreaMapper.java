package org.example.mapper.mysql1;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.example.entity.Area;

@Mapper
public interface AreaMapper extends BaseMapper<Area> {
}
