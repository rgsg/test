package org.example.mapper.mysql1;


//import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.example.entity.User;

//@DS("master")
public interface UserMapper extends BaseMapper<User> {

    void lists(User user);

    @Insert("insert into user(user_name, class_id) VALUES (#{userName}, #{classId} )")
    int insert(User user);
}
