package org.example.mapper.mysql1;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.statemachine.entity.Order;

public interface OrderMapper extends BaseMapper<Order> {
}
