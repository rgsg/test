package org.example.mapper.mysql1;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.example.entity.ClassRoom;
import org.example.entity.vo.ClassRoomVo;
import org.example.resulthandler.GroupCollectionResultHandler;

import java.util.List;

@Mapper
public interface ClassRoomMapper extends BaseMapper<ClassRoom> {
    /**
     * insert record to table
     * @param record the record
     * @return insert count
     */
    int insert(ClassRoom record);

    /**
     * insert record to table selective
     * @param record the record
     * @return insert count
     */
    int insertSelective(ClassRoom record);

    List<ClassRoomVo> getMyById(ClassRoomVo classRoomVo);

    void getMap(GroupCollectionResultHandler groupCollectionResultHandler);

    @Insert("insert into test2.test values('22','ping.cai')")
    void otherInsert();

}