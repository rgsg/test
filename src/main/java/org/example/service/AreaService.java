package org.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.entity.Area;

public interface AreaService extends IService<Area> {
    void saveOne(Area area);
}
