package org.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.entity.User;

import java.util.concurrent.ExecutionException;

public interface UserService extends IService<User> {
     void myUpdate(User zs);

    void testt() ;

    void batchInsert() throws ExecutionException, InterruptedException;
}
