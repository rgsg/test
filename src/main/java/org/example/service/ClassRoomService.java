package org.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.entity.ClassRoom;
import org.example.entity.vo.ClassRoomVo;

import java.util.List;
import java.util.Map;

public interface ClassRoomService extends IService<ClassRoom> {

    List<ClassRoomVo> get();

    Map<String, Object> getMap(Integer id);
}
