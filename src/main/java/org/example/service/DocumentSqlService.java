package org.example.service;

/**
 * @Description 数据库生成文档
 * @Date 2023-02-15 21:33
 * @Version 1.0.0
 **/

public interface DocumentSqlService {

    /**
     * 生成  数据库表结构文档
     * @date 2023-02-15 20:34
     */
    void generatorDocument();
}

