package org.example.service;

import org.example.entity.MtEquipment;
public interface MtEquipmentService{

    int deleteByPrimaryKey(Integer id);

    int insert(MtEquipment record);

    int insertSelective(MtEquipment record);

    MtEquipment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(MtEquipment record);

    int updateByPrimaryKey(MtEquipment record);

}
