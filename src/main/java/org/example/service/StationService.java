package org.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.example.entity.Station;

public interface StationService extends IService<Station> {
}
