package org.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.entity.Area;
import org.example.mapper.mysql1.AreaMapper;
import org.example.service.AreaService;
import org.example.service.StationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author cai ping
 * @description:
 * @date 2024-04-28
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements AreaService {
    @Resource
    StationService stationService;

    /**
     *
     * @Transactional 注解会拦截当前方法，创建一个DataSourceTransactionManage，绑定一个Connection，并且重置自动提交为false
     * 并放入到threadLocal中，后续的数据库操作都是根据DataSource获取connection
     * 当所有方法执行完成后，再将自动提交重置为true
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOne(Area area) {
        this.save(area);
//        Station station = new Station();
//        station.setStationId(new Random().nextInt());
//        station.setStationName("ceshi");
//        stationService.save(station);
        System.out.println("-----------area-----------");
    }
}
