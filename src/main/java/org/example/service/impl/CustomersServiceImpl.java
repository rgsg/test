package org.example.service.impl;

import org.springframework.stereotype.Service;

import org.springframework.beans.factory.annotation.Autowired;

import org.example.mapper.mysql1.CustomersMapper;
import org.example.entity.Customers;
import org.example.service.CustomersService;
@Service
public class CustomersServiceImpl implements CustomersService{

    @Autowired
    private CustomersMapper customersMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return customersMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Customers record) {
        return customersMapper.insert(record);
    }

    @Override
    public int insertSelective(Customers record) {
        return customersMapper.insertSelective(record);
    }

    @Override
    public Customers selectByPrimaryKey(Integer id) {
        return customersMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Customers record) {
        return customersMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Customers record) {
        return customersMapper.updateByPrimaryKey(record);
    }

}
