package org.example.service.impl;

import com.baomidou.dynamic.datasource.annotation.DSTransactional;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.entity.Area;
import org.example.entity.ClassRoom;
import org.example.entity.Dict;
import org.example.entity.User;
import org.example.mapper.mysql1.AreaMapper;
import org.example.mapper.mysql1.ClassRoomMapper;
import org.example.mapper.mysql1.UserMapper;
import org.example.service.AreaService;
import org.example.service.TestService;
import org.example.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

/**
 * @author cai ping
 * @description:
 * @date 2023-11-29 11:41:56
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void myUpdate(User zs) {
//        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
//        dataSourceTransactionManager.setDataSource(dataSource);
//        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
//        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
//        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);

//        QueryWrapper<User> wrapper = new QueryWrapper<>();
//        wrapper.eq("name","张三");
//        baseMapper.delete(wrapper);
////        dataSourceTransactionManager.rollback(transactionStatus);
//
//        int i = 10 / 0;
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("name", "张三");
        baseMapper.delete(wrapper);
//        dataSourceTransactionManager.rollback(transactionStatus);

    }

//    @Resource
//    UserMapper userMapper;
//
//    @Resource
//    DictMapper dictMapper;

    @Resource
    private TestService testService;

    @Resource
    private AreaService areaService;

    @Resource
    private AreaMapper areaMapper;

    @Override
    // test数据源
    @Transactional(rollbackFor = Exception.class)
//    @DSTransactional
    public void testt() {

//         test2 数据源 slave_1
//        Test test = new Test();
//        test.setName("测试1");
//        Test test1 = new Test();
//        test1.setName("测试2");
//        // 不会回滚 放入test2 数据源 连接会话
//        testService.saveBatch(Arrays.asList(test1, test));

        // 能回滚
        Area area = new Area();
        area.setName("aaa");
        area.setAreaCode("aaa");

        // test数据源
        areaMapper.insert(area);
//
        User user1 = new User();
        user1.setUserName("haha");
        // test数据源
        this.saveBatch(Collections.singletonList(user1));
//
//        System.out.println("-------------方法调用结束-------------");
//
//        throw new RuntimeException();
    }
    // 效益测算定时任务

    @Resource
    private ClassRoomMapper classRoomMapper;

    @Resource
    private PlatformTransactionManager transactionManager;

    @Override
//    @Transactional(rollbackFor = Exception.class)
    @DSTransactional
    public void batchInsert() throws ExecutionException, InterruptedException {

        ClassRoom classRoom = new ClassRoom();
        classRoom.setName("classRoom");
        classRoomMapper.insert(classRoom);

        classRoomMapper.otherInsert();


    }


    @Transactional(rollbackFor = Exception.class)
    public void addDict() {
        Dict dict = new Dict();
//        dictMapper.insert(dict);
        throw new RuntimeException("系统异常");
    }

}
