package org.example.service.impl;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author cai ping
 * @description:
 * @date 2024-06-04
 */
@Service
//@RefreshScope
public class RefreshService implements ApplicationContextAware {

    @PostConstruct
    public void init(){
        System.out.println("RefreshService 被重新创建了");
    }

//    @Value("${topic}")
    private List<String> topics;

    public List<String> getTopics() {
        return topics;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println(applicationContext);
    }
}
