package org.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.entity.Station;
import org.example.mapper.mysql1.StationMapper;
import org.example.service.StationService;
import org.springframework.stereotype.Service;

/**
 * @author cai ping
 * @description:
 * @date 2024-05-07
 */
@Service
public class StationServiceImpl extends ServiceImpl<StationMapper, Station> implements StationService {
}
