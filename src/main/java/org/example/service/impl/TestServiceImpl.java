package org.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.example.entity.Test;
import org.example.mapper.test2.TestMapper;
import org.example.service.TestService;
import org.springframework.stereotype.Service;

/**
 * @author cai ping
 * @description:
 * @date 2024-09-11
 */
@Service
//@DS("slave_1")
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {
}
