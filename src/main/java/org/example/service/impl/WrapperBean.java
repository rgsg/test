package org.example.service.impl;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-17
 */
@Component
//@MyRefreshScope
public class WrapperBean {

    @Resource
    private RefreshService refreshService;

    public void get(){
        refreshService.getTopics();
    }



}
