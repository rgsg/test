package org.example.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.SneakyThrows;
import org.example.entity.ClassRoom;
import org.example.entity.vo.ClassRoomVo;
import org.example.mapper.mysql1.ClassRoomMapper;
import org.example.resulthandler.GroupCollectionResultHandler;
import org.example.service.ClassRoomService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class ClassRoomServiceImpl extends ServiceImpl<ClassRoomMapper, ClassRoom> implements ClassRoomService {


    @Override
    @SneakyThrows
    public List<ClassRoomVo> get() {


        baseMapper.selectList(Wrappers.<ClassRoom>lambdaQuery().eq(ClassRoom::getId,1)
        );

        return Collections.emptyList();
    }

    @Override
    public Map<String, Object> getMap(Integer id) {
        GroupCollectionResultHandler resultHandler = new GroupCollectionResultHandler();
        baseMapper.getMap(resultHandler);

        return resultHandler.getResultMap();
    }

}
