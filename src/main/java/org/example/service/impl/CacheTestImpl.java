package org.example.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.example.annotation.MyCache;
import org.example.entity.Student;
import org.example.service.CacheTest;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * @author cai ping
 * @description:
 * @date 2024-08-15
 */
@Slf4j
@Service
public class CacheTestImpl implements CacheTest {
    @Override
//    @Cacheable(cacheNames = "test-cache",key = "'test'")
    @MyCache(cacheNames = "test-cache",key = "'test'",expire = 100)
    public List<String> cacheTest() {

        log.info("模拟进行数据库调用中.......");

        String string = "haha";

        log.info("调用成功......");
        return Collections.singletonList(string);
    }

    @Override
    @MyCache(cacheNames = {"studentCache","studentCache2"},expire = 500)
    public Student cacheTest2() {

        Student student = new Student();
        student.setName("caiping");
        student.setAge("20");
        return student;
    }


}
