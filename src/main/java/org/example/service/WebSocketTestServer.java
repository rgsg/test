package org.example.service;

import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

/**
 * @author cai ping
 * @description:
 * @date 2024-02-23
 */
@ServerEndpoint("/socket/{userId}")
@Component
public class WebSocketTestServer {


    @OnOpen
    public void onOpen() {
        System.out.println("连接成功");
    }

    @OnClose
    public void onClose() {

    }
    /**
     * 收到客户端消息后调用的方法
     * javax.websocket.Session
     * @param message 客户端发送过来的消息*/
    @OnMessage
    public void onMessage(@PathParam("userId") String userId, String message, Session session) {
        //群发消息
        System.out.println(message);

    }
    @OnError
    public void onError(@PathParam("userId") String userId, Throwable error) {

        error.printStackTrace();
    }


}
