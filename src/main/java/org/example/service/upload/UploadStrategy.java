package org.example.service.upload;

import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

/**
 * 文件上传策略
 */
public interface UploadStrategy {

    String uploadFile(MultipartFile file, String path);

    String uploadFile(String fileName, InputStream inputStream,String path);

}
