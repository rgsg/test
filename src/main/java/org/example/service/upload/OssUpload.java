package org.example.service.upload;

import org.example.annotation.UploadComponent;

import java.io.InputStream;

/**
 * @author cai ping
 * @description:
 * @date 2024-05-11
 */
@UploadComponent
public class OssUpload extends AbstractUpload{
    public OssUpload(){
        System.out.println("load OssUpload");
    }

    @Override
    String upload(String path, String fileName, InputStream inputStream) {
        return null;
    }

    @Override
    String getAccessUrl(String path) {
        return null;
    }
}
