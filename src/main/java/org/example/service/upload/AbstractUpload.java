package org.example.service.upload;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author cai ping
 * @description: 抽象文件上传类
 * @date 2024-05-11
 */
@Slf4j
public abstract class AbstractUpload implements UploadStrategy {

    @Override
    public String uploadFile(MultipartFile file, String path) {
        String fileName = file.getOriginalFilename();

        try {
            upload(path,fileName,file.getInputStream());
        } catch (IOException e) {
            log.error("文件上传失败:{}",fileName);
            throw new RuntimeException(e);
        }
        return getAccessUrl(path + fileName);
    }

    @Override
    public String uploadFile(String fileName, InputStream inputStream, String path) {
        upload(path,fileName,inputStream);
        return getAccessUrl(path + fileName);
    }

    abstract String upload(String path, String fileName, InputStream inputStream);

    abstract String getAccessUrl(String path);
}
