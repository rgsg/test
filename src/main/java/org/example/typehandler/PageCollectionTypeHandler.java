package org.example.typehandler;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author cai ping
 * @description:
 * @date 2024-10-18
 */
public class PageCollectionTypeHandler<E> extends BaseTypeHandler<E> {

    private static String column = "GROUP_CONCAT(%s)";
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, E parameter, JdbcType jdbcType) throws SQLException {

    }

    @Override
    public E getNullableResult(ResultSet rs, String columnName) throws SQLException {

        return null;
    }

    @Override
    public E getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return null;
    }

    @Override
    public E getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return null;
    }
}
