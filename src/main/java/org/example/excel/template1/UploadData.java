package org.example.excel.template1;

import lombok.Data;

/**
* @description: 
* @author cai ping
* @date 2023-12-11
*/
@Data
public class UploadData {
    private String acquisitionTime;
    private Object data;
    private String deviceCode;

}
