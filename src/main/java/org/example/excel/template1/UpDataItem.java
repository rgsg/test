package org.example.excel.template1;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author cai ping
 * @description:
 * @date 2023-12-11
 */
@Data
public class UpDataItem {

    @ExcelProperty(index = 1)
    private String acquisitionTime;
    @ExcelProperty(index = 2)
    private Long data1;
    @ExcelProperty(index = 3)
    private Long data2;

//    @ExcelProperty(index = 4)
//    private String data3;
    @ExcelIgnore
    private String deviceCode;


}
