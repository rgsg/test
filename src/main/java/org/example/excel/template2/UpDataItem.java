package org.example.excel.template2;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author cai ping
 * @description:
 * @date 2023-12-11
 */
@Data
public class UpDataItem {

    @ExcelProperty(index = 2)
    private String acquisitionTime;
    @ExcelProperty(index = 3)
    private Long data1;
    @ExcelIgnore
    private String deviceCode;


}
