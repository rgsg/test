package org.example.excel.template2;

import com.alibaba.excel.EasyExcel;

import java.util.Scanner;

/**
 * @author cai ping
 * @description:
 * @date 2023-12-11
 */
public class ExcelRead {
    public static void main(String[] args) {
        //读取文件
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入表格地址:");
        String filePath = scanner.nextLine();
        EasyExcel.read(filePath, UploadData.class,new DataListener()).headRowNumber(1).sheet(1).doRead();
    }
}
