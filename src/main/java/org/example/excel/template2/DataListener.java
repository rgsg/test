package org.example.excel.template2;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.fastjson.JSONObject;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author cai ping
 * @description:
 * @date 2023-12-11
 */
public class DataListener extends AnalysisEventListener<UpDataItem> {

    private Map<String, List<UpDataItem>> dataItemMap = new HashMap<>(30);

    private static Map<String,String> nameMap = new HashMap<>();

    static {
        nameMap.put("西江监狱","010102767");
        nameMap.put("港北区第二初级中学","010155418");
        nameMap.put("西江高级中学","010106779");
        nameMap.put("公园事务中心","010105952");
        nameMap.put("港北高级中学","010099314");
        nameMap.put("维多利亚酒店有限公司","010089397");
        nameMap.put("御珑置业有限公司","010111534");
        nameMap.put("鑫益新磷化工有限公司","010000156");
        nameMap.put("第四初级中学","010098764");
        nameMap.put("公安局巡警支队","010062391");

    }
    private List<UploadData> upDataItems = new ArrayList<>();
    @Override
    public void invoke(UpDataItem data, AnalysisContext context) {
        ReadSheet readSheet = context.readSheetHolder().getReadSheet();
        String sysCode = readSheet.getHead().get(0).get(0);
        //第一条
        HashMap<String, Long> hashMap = new HashMap<>();
        hashMap.put("LJLL_CS", data.getData1());
        UploadData uploadData = new UploadData();
        uploadData.setData(hashMap);
        uploadData.setDeviceCode(sysCode);
        uploadData.setAcquisitionTime(formatTime(data.getAcquisitionTime()));
        //第二条
//        HashMap<String, Long> hashMap2 = new HashMap<>();
//        hashMap2.put("LJLL_CS", data.getData2());
//        UploadData uploadData2 = new UploadData();
//        uploadData2.setData(hashMap2);
//        uploadData2.setDeviceCode(sysCode);
//        uploadData2.setAcquisitionTime(formatTime2(data.getAcquisitionTime()));


        upDataItems.add(uploadData);
//        upDataItems.add(uploadData2);
    }


    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        System.out.println(JSONObject.toJSONString(upDataItems));
    }

    public String formatTime(String dateTime) {
        Date date = new Date(dateTime);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        String time = simpleDateFormat.format(date);
        return time;
    }
    public String formatTime2(String dateTime) {
        Date date = new Date(dateTime);
        date.setHours(23);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        String time = simpleDateFormat.format(date);
        return time;
    }
}
