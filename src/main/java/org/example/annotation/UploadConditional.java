package org.example.annotation;

import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.core.type.ClassMetadata;

/**
 * @author cai ping
 * @description:
 * @date 2024-05-11
 */
public class UploadConditional extends SpringBootCondition {
    @Override
    public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
        if (!(metadata instanceof ClassMetadata)) {
            return new ConditionOutcome(false, "not class");
        }
        String type = context.getEnvironment().getProperty("upload.mode");
        ClassMetadata classMetadata = (ClassMetadata) metadata;
        String className = classMetadata.getClassName();
        if(type != null && className.endsWith(type)){
            return ConditionOutcome.match();
        }
        return new ConditionOutcome(false, "not class");
    }
}
