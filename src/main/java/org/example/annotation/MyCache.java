package org.example.annotation;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

@Retention(RetentionPolicy.RUNTIME)
@Cacheable
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface MyCache {

    @AliasFor(annotation = Cacheable.class,attribute = "value")
    String[] value() default {};

    @AliasFor(annotation = Cacheable.class,attribute = "cacheNames")
    String[] cacheNames() default {};

    @AliasFor(annotation = Cacheable.class,attribute = "key")
    String key() default "";

    @AliasFor(annotation = Cacheable.class,attribute = "keyGenerator")
    String keyGenerator() default "";

    @AliasFor(annotation = Cacheable.class,attribute = "cacheManager")
    String cacheManager() default "";

    @AliasFor(annotation = Cacheable.class,attribute = "cacheResolver")
    String cacheResolver() default "";

    @AliasFor(annotation = Cacheable.class,attribute = "condition")
    String condition() default "";

    @AliasFor(annotation = Cacheable.class,attribute = "unless")
    String unless() default "";

    @AliasFor(annotation = Cacheable.class,attribute = "sync")
    boolean sync() default false;

    int expire() default  1;

    TimeUnit unit() default TimeUnit.DAYS;


}
