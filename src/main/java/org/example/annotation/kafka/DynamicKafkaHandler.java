package org.example.annotation.kafka;

import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-30
 */
//@Component
public class DynamicKafkaHandler implements SmartInitializingSingleton, BeanPostProcessor {

    @Override
    public void afterSingletonsInstantiated() {
    }
}
