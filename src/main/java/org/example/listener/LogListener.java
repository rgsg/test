package org.example.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.stereotype.Component;

/**
 * @author cai ping
 * @description:
 * @date 2024-04-16
 */
@Component
@Slf4j
public class LogListener{
    @EventListener(ApplicationReadyEvent.class)
    public void logListener(ApplicationReadyEvent event) {
        ConfigurableEnvironment environment = event.getApplicationContext().getEnvironment();
        String port = environment.getProperty("server.port");
        log.info("server.port={}",port);
        System.out.println(event + " ----------------");
    }
}
