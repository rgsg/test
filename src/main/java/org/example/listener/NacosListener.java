package org.example.listener;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author cai ping
 * @description:
 * @date 2024-06-21
 */
@Component
public class NacosListener {
    @Resource
    Environment environment;

//    @Resource
//    @Lazy
//    private KafkaListenerEndpointRegistry consumerRegistry;
//
//    @EventListener(RefreshScopeRefreshedEvent.class)
//    public void setConfig() {
//        String topic = environment.getProperty("kafka.topic");
//        MessageListenerContainer tsData = consumerRegistry.getListenerContainer("tsData");
//        tsData.stop();
//        ContainerProperties containerProperties = tsData.getContainerProperties();
//        Field topicsField = ReflectUtil.getField(containerProperties.getClass(), "topics");
//        ReflectUtil.setFieldValue(containerProperties, topicsField, topic);
//        tsData.start();
//        System.out.println(topic);
//    }
}
