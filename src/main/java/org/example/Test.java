package org.example;


import com.alibaba.fastjson.parser.deserializer.ExtraProcessor;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.ToString;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.example.api.Hello;
import org.example.api.HelloServiceGrpc;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-24
 */
@Validated
@Component
public class Test {
    public static String sayHaha() {
        System.out.println("haha");
        return "haha";
    }

    public String test(@NotNull String name) {
        System.out.println(name);
        return "";
    }

    @SneakyThrows
    public static void main(String[] args) {

        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 3333)
                .usePlaintext()
                .build();

        HelloServiceGrpc.HelloServiceFutureStub futureStub = HelloServiceGrpc.newFutureStub(channel);

        ListenableFuture<Hello.HelloResp> ccc = futureStub.hello(Hello.HelloReq.newBuilder().setName("ccc").build());

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        Futures.addCallback(ccc, new FutureCallback<Hello.HelloResp>() {

            @Override
            public void onSuccess(Hello.@Nullable HelloResp result) {
                System.out.println("result.getResult() = " + result.getResult());
            }

            @Override
            public void onFailure(Throwable t) {

            }
        },executorService);

        System.out.println("resp.getResult() = ");

        TimeUnit.SECONDS.sleep(5);

        channel.shutdown();
        executorService.shutdown();

    }

    @Data
    @ToString
    public static class Device {

        private String createBy;

        private String industry;

        private String instructions;

        private String deviceNo;

    }

    public static class MyExtraProcessor implements ExtraProcessor {


        @Override
        public void processExtra(Object object, String key, Object value) {
            System.out.println("object = " + object);
            System.out.println("key = " + key);
            System.out.println("value = " + value);
        }
    }
}
