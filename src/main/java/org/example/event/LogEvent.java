package org.example.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author cai ping
 * @description:
 * @date 2024-04-16
 */
public class LogEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public LogEvent(Object source) {
        super(source);
    }
}
