package org.example.entity;

import lombok.Data;

/**
 * 测试
 */
@Data
public class Test {
    private Integer id;

    private String name;
}