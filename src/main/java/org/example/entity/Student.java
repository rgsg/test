package org.example.entity;

import lombok.Data;

/**
 * @author cai ping
 * @description:
 * @date 2023-12-14
 */
@Data
public class Student {
    private String name;

    private String age;
}
