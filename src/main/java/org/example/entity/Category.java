package org.example.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

/**
 * @author cai ping
 * @description:
 * @date 2024-05-14
 */
@TableName(value = "pms_category")
@Data
public class Category {
    private Integer catId;

    private String name;

    private Integer parentCid;

    private Integer catLevel;

    private Integer showStatus;

    private Integer sort;

    private String icon;

    private String productUnit;

    private String productCount;

    @TableField(exist = false)
    private List<Category> children;
}
