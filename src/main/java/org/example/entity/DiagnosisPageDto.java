package org.example.entity;

import lombok.Data;

/**
 * @author cai ping
 * @description:
 * @date 2023-12-12
 */
@Data
public class DiagnosisPageDto {


    /**
     * 租户号
     */
    private String ownership;

    /**
     * 处理状态 0未处理 ； 10 已消缺； 20已建单
     */
    private Integer disposeStatus;

    /**
     * 报警等级ID
     */
    private String alarmLevelId;


    /**
     * 组织编号
     */
    private Long orgId;



    /**
     * 规则类型
     */
    private String ruleTypeCode;

    /**
     * 组织类型code 0 组织id 1 设备厂家
     */
    private String orgTypeCode;


}
