package org.example.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class MtEquipment {
    /**
    * 主键ID
    */
    private Long id;

    /**
    * 兼容老iot
    */
    private String code;

    /**
    * 租户
    */
    private String ownership;

    /**
    * 设备类别ID
    */
    private String mtEquipmentClassId;

    /**
    * 设备名称
    */
    private String equipmentName;

    /**
    * 设备物理编号、通信地址、唯一编码
    */
    private String equipmentId;

    /**
    * 设备逻辑编号、表号
    */
    private String equipmentNum;

    /**
    * imsi\imei
    */
    private String imsi;

    /**
    * imei
    */
    private String imei;

    /**
    * 区域ID
    */
    private Long mtAreaId;

    /**
    * 经度
    */
    private BigDecimal longitude;

    /**
    * 纬度
    */
    private BigDecimal latitude;

    /**
    * 表经纬度位置
    */
    private String positionValue;

    /**
    * 设备安装位置
    */
    private String position;

    /**
    * 位置备注信息
    */
    private String positionDesc;

    /**
    * 数据状态  1 启用 0 停用
    */
    private Boolean state;

    /**
    * 数据记录新增时间
    */
    private Date createTime;

    /**
    * 数据记录最后一次修改时间
    */
    private Date updateTime;

    /**
    * 产品ID
    */
    private Integer productId;

    /**
    * 产品CODE
    */
    private String productCode;

    /**
    * 产品名称
    */
    private String productName;

    /**
    * 用户名称
    */
    private String userName;

    /**
    * 地址
    */
    private String address;

    /**
    * 供应商CODE
    */
    private String supplierCode;

    /**
    * 表供应商信息
    */
    private String supplierInfo;

    /**
    * 删除标记  0 正常  1已删除
    */
    private Boolean delFlag;

    /**
    * 表具参数ID
    */
    private Long meterModelPkid;

    /**
    * 序号
    */
    private Integer seq;

    /**
    * 口径
    */
    private String caliber;

    /**
    * 表序号
    */
    private String meterSerialNum;

    /**
    * 集中器号
    */
    private String concentratorNum;

    /**
    * 分流端口号
    */
    private String collectPort;

    /**
    * 设备类型,1小表，2大表，3集中器
    */
    private Byte deviceType;

    /**
    * 是否监测表（0：是；1：不是）
    */
    private Boolean monitor;

    /**
    * 安装状态(0:已安装，1:未安装)
    */
    private Boolean installState;

}