package org.example.entity.vo;

import lombok.Data;
import org.example.entity.User;

import java.util.List;

/**
 * @author cai ping
 * @description:
 * @date 2024-10-16
 */
@Data
public class ClassRoomVo  {

    private Integer id;

    private String name;

    private List<User> users;

}
