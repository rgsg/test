package org.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@TableName("s_dict")
@Data
public class Dict {

    /**
     * id
     */
    @TableId(value = "dict_id", type = IdType.AUTO)
    private Integer dictId;

    /**
     * 分类名称
     */
    private String sortName;

    /**
     * 分类编码
     */
    private String sortCode;

    /**
     * 编码名称
     */
    private String name;

    /**
     * 编码值
     */
    private String defaultValue;

    /**
     * 状态 1 有效 0 无效
     */
    private Integer status;

    /**
     * 排序
     */
    private Integer sortNo;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime modifyTime;



}