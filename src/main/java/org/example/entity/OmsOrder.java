package org.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author cai ping
 * @description:
 * @date 2024-05-08
 */
@Data
@TableName("oms_order")
public class OmsOrder {
    @TableId(value = "member_id",type = IdType.AUTO)
    private Integer memberId;
}
