package org.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author cai ping
 * @description:
 * @date 2024-04-11
 */
@TableName("d_distribution_record_history")
@Data
public class CkEntity {
    private String distributionSerialNo;
}
