package org.example.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Customers {

    @ExcelProperty("id")
    private Long id;

    @ExcelProperty("firstName")
    private String firstName;

    @ExcelProperty("lastName")
    private String lastName;

    @ExcelProperty("lastName")
    private String email;

    @ExcelProperty("phone")
    private String phone;

    @ExcelProperty("address")
    private String address;

    @ExcelProperty("city")
    private String city;

    @ExcelProperty("state")
    private String state;

    @ExcelProperty("country")
    private String country;

    @ExcelProperty("postalCode")
    private String postalCode;

    @ExcelProperty("age")
    private Integer age;

    @ExcelProperty("gender")
    private String gender;

    @ExcelProperty("income")
    private BigDecimal income;

    @ExcelProperty("jobTitle")
    private String jobTitle;

    @ExcelProperty("company")
    private String company;

    @ExcelProperty("createdAt")
    private Date createdAt;
}