package org.example.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author cai ping
 * @description:
 * @date 2024-05-07
 */
@Data
@TableName("p_station")
public class Station {

    private Integer stationId;

    private String stationName;
}
