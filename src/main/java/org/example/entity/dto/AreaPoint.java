package org.example.entity.dto;

import org.springframework.data.geo.Point;

/**
 * @author cai ping
 * @description:
 * @date 2025-01-23
 */
public class AreaPoint extends Point {
    public AreaPoint(double x, double y) {
        super(x, y);
    }

    public AreaPoint(Point point) {
        super(point);
    }
}
