package org.example.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author cai ping
 * @description:
 * @date 2023-11-24 15:14:11
 */
@Data
public class Water {
//    private String id;
//    private User user;
//    @JsonDeserialize(using = CodeToEnumDeserializer.class)
//    private RatedWaterEnum ratedWaterEnum;
//    @DateTimeFormat(pattern = "yyyy-MM")
//    private String dateTime;

    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;
}
