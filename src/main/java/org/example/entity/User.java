package org.example.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;

/**
 * @author caiping
 * @date 2023-08-24
 * @description 用户实体
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private String userName;

    @TableId
    private Integer id;

    private Integer classId;

}