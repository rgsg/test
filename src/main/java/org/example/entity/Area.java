package org.example.entity;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author cai ping
 * @description:
 * @date 2024-03-12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("p_area")
public class Area  {
    @TableId(value = "area_id",type = IdType.AUTO)
    private Integer id;
    @NotNull
    private String name;
    private String areaCode;

//    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date createTime;

    private Date modifyTime = DateUtil.date();

    public Area(String name, String code) {
        this.name = name;
        this.areaCode = code;
    }
}
