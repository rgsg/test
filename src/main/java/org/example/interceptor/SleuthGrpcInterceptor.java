package org.example.interceptor;

import io.grpc.*;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

/**
 * @author cai ping
 * @description:
 * @date 2025-01-10
 */
//@GrpcGlobalClientInterceptor
public class SleuthGrpcInterceptor implements ClientInterceptor {
    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> method, CallOptions callOptions, Channel next) {


        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(
                next.newCall(method, callOptions)) {
            @Override
            public void start(Listener<RespT> responseListener, Metadata headers) {
                // 添加traceId
                String trackId = MDC.get("traceId");
                if (!StringUtils.isEmpty(trackId)) {
                    headers.put(Metadata.Key.of("traceId", Metadata.ASCII_STRING_MARSHALLER), trackId);
                }
                super.start(
                        new ForwardingClientCallListener.SimpleForwardingClientCallListener<RespT>(
                                responseListener) {
                            @Override
                            public void onHeaders(Metadata headers) {
                                super.onHeaders(headers);
                            }
                        },
                        headers);
            }
        };
    }
}
