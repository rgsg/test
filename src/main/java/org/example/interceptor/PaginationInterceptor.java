package org.example.interceptor;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.example.utils.PageUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * @author cai ping
 * @description:
 * @date 2024-05-11
 */
@Component
public class PaginationInterceptor implements HandlerInterceptor {
    String CURRENT = "current";
    String SIZE = "size";
    String DEFAULT_SIZE = "10";
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String parameter = Optional.ofNullable(request.getParameter(CURRENT)).orElse("1");
        String pageSize = Optional.ofNullable(request.getParameter(SIZE)).orElse(DEFAULT_SIZE);
        PageUtil.setCurrentPage(new Page<>(Long.parseLong(parameter),Long.parseLong(pageSize)));
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        PageUtil.remove();
    }
}
