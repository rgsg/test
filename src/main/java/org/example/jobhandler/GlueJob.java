package org.example.jobhandler;

//import com.xxl.job.core.context.XxlJobHelper;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author cai ping
 * @description:
 * @date 2023-12-20
 */
@Component
@ConditionalOnExpression("${lifeline.task.enable:false}")
public class GlueJob {


//    @XxlJob(value = "gluejob")
    @Scheduled(cron = "0/1 * * * * ?")
    public void execute() throws Exception {
        System.out.println("--------------------");
//        String jobParam = XxlJobHelper.getJobParam();
//        System.out.println(jobParam);
    }
}
