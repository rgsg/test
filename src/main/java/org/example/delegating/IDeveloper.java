package org.example.delegating;

public interface IDeveloper {
    void developSoftware();
}
