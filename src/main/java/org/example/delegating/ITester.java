package org.example.delegating;

public interface ITester {
	boolean isBusyAsTester();
	void testSoftware();
}