package org.example.ognl;

import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mvel2.MVEL;

import java.util.List;

/**
 * @author cai ping
 * @description:
 * @date 2024-11-26
 */
public class TestOgnl {

    public static void main(String[] args) throws Exception {

        String str = "[{\"key\":\"key1\",\"value\":[{\"stationCode\":\"测试01\",\"value\":\"3.00\"},{\"stationCode\":\"测试02\",\"value\":\"5.00\"}]}]";

        ObjectMapper objectMapper = new ObjectMapper();
        List list = objectMapper.readValue(str, List.class);

        Object eval = MVEL.eval("value[0]", list);

        System.out.println(eval);
    }

    private static <T> List<T> parseArray(JsonNode dataNode, Class<T> dataItemClass) {
        String text = dataNode.asText();
        return JSONArray.parseArray(text, dataItemClass);
    }


    @lombok.Data
    static class StationData {
        private String stationCode;
        private String value;
    }

    @lombok.Data
    static class TokenInfo {

        private String code;

        private String info;

        private Token data;
    }

    @lombok.Data
    static class Token {

        private String token;

        private String expirationTime;

    }


    @lombok.Data
    private static class DataItem {
        //数据项key
        private String recordKey;
        //设备key
        private String deviceKey;
        //读数
        private String recordValue;
        //时间
        private String recordDate;

        public String getRecordKey() {
            return recordKey.substring(recordKey.indexOf(deviceKey));
        }
    }

}
