package org.example.ognl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.ibatis.ognl.*;
import org.apache.ibatis.scripting.xmltags.OgnlClassResolver;

import java.io.IOException;
import java.lang.reflect.Member;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * @author cai ping
 * @description: json提取器
 * @date 2024-11-26
 */
public class JsonExtraction {

    private final static ConcurrentHashMap<String,Object> expressionCache = new ConcurrentHashMap<>();

    private static final ClassResolver CLASS_RESOLVER = new OgnlClassResolver();

    private static final MemberAccess MEMBER_ACCESS = new MemberAccess() {
        @Override
        public Object setup(OgnlContext context, Object target, Member member, String propertyName) {
            return null;
        }

        @Override
        public void restore(OgnlContext context, Object target, Member member, String propertyName, Object state) {

        }

        @Override
        public boolean isAccessible(OgnlContext context, Object target, Member member, String propertyName) {
            return false;
        }

    };

    /**
     * {"code":"success","info":"","data":{"token":"f8db0749b1d7436aaa7d573f2f06ed6b","expirationTime":86400000}}
     *
     * @param responseJson http响应对象
     * @param express      获取对象属性 data.token
     * @param returnClass  返回类型
     */

    @SneakyThrows
    public static <T> T getData(String responseJson, String express, Class<T> returnClass) {
        return handlerData(responseJson, express, (value) -> JSONObject.parseObject(value, returnClass));
    }

    @SneakyThrows
    public static <T> T getData(String responseJson, String express, TypeReference<T> returnClass) {
        return handlerData(responseJson, express, (value) -> JSONObject.parseObject(value, returnClass.getType()));
    }

    @SneakyThrows
    public static <T> List<T> getArrayData(String responseJson, String express, Class<T> returnClass) {
        return handlerData(responseJson, express, (value) -> JSONObject.parseArray(value, returnClass));
    }

    @SneakyThrows
    public static <T> T parseJsonArray(String responseJson, String express, Class<T> returnClass) {
        return handlerJsonArrayData(responseJson,express,(value) -> JSONObject.parseObject(value,returnClass));
    }

    /**
     * 获取编译后的表达式
     */
    public static Object getCompiledExpression(String expression) {
        return expressionCache.computeIfAbsent(expression, expr -> {
            try {
                return Ognl.parseExpression(expr);
            } catch (Exception e) {
                throw new RuntimeException("Invalid OGNL expression: " + expr, e);
            }
        });
    }

    private static <R> R handlerJsonArrayData(String responseJson, String express, Function<String, R> function) throws OgnlException, IOException {

        ObjectTypeConverter converter = new ObjectTypeConverter();

        OgnlContext ognlContext = new OgnlContext(CLASS_RESOLVER, converter, MEMBER_ACCESS);

        ObjectMapper objectMapper = new ObjectMapper();
        List<Map<String, Object>> listMap = objectMapper.readValue(responseJson, new TypeReference<List<Map<String, Object>>>() {
        });

        Ognl.getValue(getCompiledExpression(express), ognlContext, listMap, String.class);

        return function.apply(converter.getValue());
    }

    private static <R> R handlerData(String responseJson, String express, Function<String, R> function) throws OgnlException, IOException {

        ObjectTypeConverter converter = new ObjectTypeConverter();

        OgnlContext ognlContext = new OgnlContext(CLASS_RESOLVER, converter, MEMBER_ACCESS);

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> jsonMap = objectMapper.readValue(responseJson, new TypeReference<Map<String, Object>>() {
        });

        Ognl.getValue(getCompiledExpression(express), ognlContext, jsonMap, String.class);

        return function.apply(converter.getValue());
    }


    @Getter
    public static class ObjectTypeConverter implements TypeConverter {

        private String value;

        @Override
        public Object convertValue(OgnlContext context, Object target, Member member, String propertyName, Object value, Class<?> toType) {
            this.value = JSON.toJSONString(value);
            return null;
        }
    }

}
