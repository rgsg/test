package org.example.resulthandler;

import lombok.Getter;
import org.apache.ibatis.session.ResultContext;
import org.apache.ibatis.session.ResultHandler;
import org.apache.poi.ss.formula.functions.T;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cai ping
 * @description: 分组集合结果处理器
 * @date 2024-10-21
 */
@Getter
public class GroupCollectionResultHandler implements ResultHandler<T> {

    private Map<String,Object> resultMap = new HashMap<>();
    @Override
    public void handleResult(ResultContext resultContext) {
        Map<String,Object> resultObject = (Map) resultContext.getResultObject();
        resultMap.putAll(resultObject);
        Object resultObject1 = resultContext.getResultObject();
    }

}
