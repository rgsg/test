package org.example.enums;


public enum RatedWaterEnum {
    ALL(-1,"全部用户"), NORMAL(100,"正常用水用户"),EXCESS(200,"超额用水用户");
    private int status;
    private String description;

    RatedWaterEnum(int status, String description) {
        this.status = status;
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
    public static <T extends RatedWaterEnum> T getEnum(Class<T> type, int code) {
        T[] objs = type.getEnumConstants();
        for (T em : objs) {
            if (em.getStatus() == (code)) {
                return em;
            }
        }
        return null;
    }

}