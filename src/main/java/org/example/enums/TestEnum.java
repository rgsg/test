package org.example.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Setter;

/**
 * @author cai ping
 * @description:
 * @date 2023-12-25
 */
@AllArgsConstructor
public enum TestEnum implements BaseEnum {
    RANDOM("100","随机值"),ACCUMULATE("1000","累计值");

    // 0 随机 1 累计
    @Setter
    @EnumValue
    private String code;

    // 描述
    @Setter
    private String description;

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }

    public static <T extends TestEnum> T getEnum(Class<T> type, String code) {
        T[] objs = type.getEnumConstants();
        for (T em : objs) {
            if (em.getCode().equals (code)) {
                return em;
            }
        }
        return null;
    }
}
