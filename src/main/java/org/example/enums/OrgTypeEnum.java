package org.example.enums;

import lombok.AllArgsConstructor;
import org.example.strategy.DiagnosisStrategy;
import org.example.strategy.impl.DiagnosisDeviceStrategyImpl;
import org.example.strategy.impl.DiagnosisOrgStrategyImpl;

/**
 * @author cai ping
 * @description:
 * @date 2024-01-11
 */
@AllArgsConstructor
public enum OrgTypeEnum {
    DEVICE(new DiagnosisDeviceStrategyImpl()),ORG(new DiagnosisOrgStrategyImpl());
//
    private DiagnosisStrategy diagnosisStrategy;
}
