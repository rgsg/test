package org.example.enums;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

/**
 * 表单BaseEnum枚举转换
 */
public class BaseEnumConverterFactory implements ConverterFactory<String, TestEnum> {


    @Override
    public <T extends TestEnum> Converter<String, T> getConverter(Class<T> targetType) {
        return new BaseEnumConverter(targetType);
    }

    public  static class BaseEnumConverter<String, T extends TestEnum> implements Converter<java.lang.String, TestEnum> {

        private Class<T> type;

        public BaseEnumConverter(Class<T> type) {
            this.type = type;
        }

        @Override
        public TestEnum convert(java.lang.String source) {
            return TestEnum.getEnum(type, source);
        }
    }
}

