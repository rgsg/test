//package org.example.plugin;
//
//import lombok.extern.slf4j.Slf4j;
//import org.apache.ibatis.executor.statement.StatementHandler;
//import org.apache.ibatis.mapping.BoundSql;
//import org.apache.ibatis.plugin.Interceptor;
//import org.apache.ibatis.plugin.Intercepts;
//import org.apache.ibatis.plugin.Invocation;
//import org.apache.ibatis.plugin.Signature;
//
//import java.sql.Connection;
//
//@Intercepts(value = @Signature(
//        type = StatementHandler.class,
//        method = "prepare",
//        args = {Connection.class,Integer.class}
//))
////@Component
//@Slf4j
//public class SqlPrintPlugin implements Interceptor {
//    @Override
//    public Object intercept(Invocation invocation) throws Throwable {
//        StatementHandler target = (StatementHandler) invocation.getTarget();
//        BoundSql boundSql = target.getBoundSql();
//        log.info("执行sql[{}]",boundSql.getSql());
//        return invocation.proceed();
//    }
//
//    @Override
//    public Object plugin(Object target) {
//        return Interceptor.super.plugin(target);
//    }
//}
