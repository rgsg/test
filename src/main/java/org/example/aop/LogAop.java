package org.example.aop;

import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.MDC;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * @author cai ping
 * @description:
 * @date 2023-12-18
 */
//@EnableAspectJAutoProxy
//@Aspect
//@Component
@Slf4j
public class LogAop {


    @Pointcut(value = "execution(* org.example.controller.*.*(..))")
    public void recordingTimePointCut() {
    }

    @Around("recordingTimePointCut()")
    public Object recordingTime(ProceedingJoinPoint joinPoint) throws Throwable {

        long beginTime = System.currentTimeMillis();
        Signature signature = joinPoint.getSignature();
        String[] parameterNames = ((CodeSignature) signature).getParameterNames();
        String methodName = signature.getDeclaringTypeName() + "." + signature.getName();
        String methodSignature = spellMethodSignature(methodName, parameterNames);

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("requestId", IdUtil.fastSimpleUUID());
        MDC.setContextMap(hashMap);

        log.info("开始执行方法 [{}]", methodSignature);

        Object proceed = null;
        long endTime;
        try {
            proceed = joinPoint.proceed();
        } catch (Throwable e) {
            endTime = System.currentTimeMillis();
            log.error("方法执行异常[{}]，耗时 [{}] 毫秒", methodSignature, endTime - beginTime,e);
        }
        endTime = System.currentTimeMillis();
        log.info("方法执行完毕[{}]，耗时 [{}] 毫秒", methodSignature, endTime - beginTime);

        new Thread(() ->

                log.info("子线程日志请求:--------")

        ).start();
        TimeUnit.SECONDS.sleep(1);
        MDC.clear();

        return proceed;
    }
    private String spellMethodSignature(String methodName, Object[] args) {

        StringBuilder stringBuilder = new StringBuilder(methodName);
        stringBuilder.append("(");
        for (int i = 0; i < args.length; i++) {
            stringBuilder.append(args[i] + ",");
        }
        return stringBuilder.substring(0, stringBuilder.length() - 1) + ")";
    }
}
