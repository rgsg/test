package org.example.aop;

import org.example.entity.vo.ClassRoomVo;
import org.example.mapper.mysql1.ClassRoomMapper;
import org.example.service.ClassRoomService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * (class_room)表控制层
 *
 * @author xxxxx
 */
@RestController
@RequestMapping("/class_room")
public class ClassRoomController {
    /**
     * 服务对象
     */
    @Resource
    private ClassRoomService classRoomService;

    @Resource
    private ClassRoomMapper classRoomMapper;

    /**
     * 通过主键查询单条数据
     *
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public List<ClassRoomVo> selectOne() {
        return classRoomService.get();
    }

    @GetMapping("getMap")
    public Map<String,Object> getMap(Integer id) {
        return classRoomService.getMap(id);
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        classRoomMapper.deleteById(id);
    }
}
