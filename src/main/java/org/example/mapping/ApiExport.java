//package org.example.mapping;
//
//import cn.hutool.extra.spring.SpringUtil;
//import com.alibaba.excel.EasyExcel;
//import com.alibaba.excel.annotation.ExcelProperty;
//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.Setter;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.method.HandlerMethod;
//import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
//import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
//
//import javax.annotation.PostConstruct;
//import javax.servlet.http.HttpServletRequest;
//import java.util.ArrayList;
//import java.util.Comparator;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
///**
// * @author cai ping
// * @description:
// * @date 2024-03-25
// */
////@Component
//@Slf4j
//public class ApiExport extends RequestMappingHandlerMapping {
//    private RequestMappingHandlerMapping handlerMapping;
//
//    @PostConstruct
//    public void init(){
//        this.setOrder(-2);
//        this.handlerMapping = SpringUtil.getBean(RequestMappingHandlerMapping.class);
//    }
//
//    @Override
//    protected HandlerMethod lookupHandlerMethod(String lookupPath, HttpServletRequest request) throws Exception {
//        ArrayList<ApiInfo> apiInfos = new ArrayList<>();
//        log.info("---------------------------------------------------------------");
//        Map<RequestMappingInfo, HandlerMethod> handlerMethods = handlerMapping.getHandlerMethods();
//        for (Map.Entry<RequestMappingInfo,HandlerMethod> entry : handlerMethods.entrySet()){
//            RequestMappingInfo requestMappingInfo = entry.getKey();
//            HandlerMethod handlerMethod = entry.getValue();
//
//            String url = requestMappingInfo.getPatternsCondition().getPatterns().stream().findFirst().get();
//            Api controllerModel = handlerMethod.getBeanType().getAnnotation(Api.class);
//            if(controllerModel == null){
//                log.info("url:{} 未正确处理",url);
//                continue;
//            }
//            ApiOperation methodName = handlerMethod.getMethodAnnotation(ApiOperation.class);
//            if(methodName == null){
//                log.info("url:{} 未正确处理",url);
//                continue;
//            }
//            RequestMethod method = requestMappingInfo.getMethodsCondition().getMethods().stream().findFirst().get();
//            ApiInfo apiInfo = new ApiInfo(controllerModel.tags()[0], methodName.value(), url, method.name());
//            apiInfos.add(apiInfo);
//        }
//        List<ApiInfo> res = apiInfos.stream().sorted(Comparator.comparing(ApiInfo::getControllerModel)).collect(Collectors.toList());
//        EasyExcel.write("d://api.xlsx", ApiInfo.class).sheet("api").doWrite(res);
//        log.info("---------------------------------------------------------------");
//        return null;
//    }
//
//    @AllArgsConstructor
//    @Getter
//    @Setter
//    class ApiInfo{
//        @ExcelProperty("模块名称")
//        private String controllerModel;
//        @ExcelProperty("方法名称")
//        private String methodName;
//        @ExcelProperty("url")
//        private String url;
//        @ExcelProperty("请求方法")
//        private String method;
//    }
//}
