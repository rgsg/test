package org.example.mapping;

import cn.hutool.extra.spring.SpringUtil;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.condition.RequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.UrlPathHelper;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cai ping
 * @description: app版本向下兼容匹配
 * @date 2024-03-13
 */
@ConditionalOnExpression("${app.multi-version:false}")
@NoArgsConstructor
@Slf4j
@Component
public class AppMultiVersion extends RequestMappingHandlerMapping {
    private Map<RequestMappingInfo, HandlerMethod> handlerMethods;

    private RequestMappingHandlerMapping handlerMapping;

    //url ---> 请求处理器映射
    private final Map<String, TreeMap<String,RequestMappingInfo>> urlLookup = new HashMap<>();

    private UrlPathHelper urlPathHelper = new UrlPathHelper();

    //真实路径正则
    private String realLookupPathRegex = "\\/v\\d+(\\.\\d+)*(\\/.*)?$";

    //获取版本
    private String versionRegex = "(?:.*\\/)?v(\\d+(\\.\\d+)*)";

    //请求头版本标识字段
    private String versionFlag = "version";

    @PostConstruct
    public void init(){
        this.setOrder(-2);
        this.handlerMapping = SpringUtil.getBean(RequestMappingHandlerMapping.class);
        handlerMethods = handlerMapping.getHandlerMethods();
        this.initUrlLookup();
    }

    @Override
    protected RequestMappingInfo createRequestMappingInfo(RequestMapping requestMapping, RequestCondition<?> customCondition) {
        return super.createRequestMappingInfo(requestMapping, customCondition);
    }

    /**
     * 初始化url与RequestMapping的映射
     */

    private void initUrlLookup() {
        for (Map.Entry<RequestMappingInfo, HandlerMethod> entry : handlerMethods.entrySet()) {

            RequestMappingInfo requestMappingInfo = entry.getKey();
            List<String> directUrls = getDirectUrls(requestMappingInfo);
            for (String url : directUrls) {
                String realLookupPath = url.replaceAll(realLookupPathRegex, "$2");
                String versionKey = getVersion(url);
                if(versionKey == null) continue;
                TreeMap<String,RequestMappingInfo> mappingInfos = this.urlLookup.getOrDefault(realLookupPath,new TreeMap<>(Collections.reverseOrder()));
                mappingInfos.put(versionKey,requestMappingInfo);
                this.urlLookup.putIfAbsent(realLookupPath,mappingInfos);
            }
        }
    }

    /**
     * 根据路径获取处理器方法
     * @param lookupPath mapping lookup path within the current servlet mapping
     * @param request the current request
     * @return
     * @throws Exception
     */
    @Override
    protected HandlerMethod lookupHandlerMethod(String lookupPath, HttpServletRequest request) throws Exception {

        try {
            String requestURI = urlPathHelper.getLookupPathForRequest(request);

            String requestVersion = request.getHeader(versionFlag);
            //请求体没带版本号以及后端url接口不需要多版本的放行
            if(requestVersion == null || getVersion(lookupPath) == null) return handleNoMatch(handlerMethods.keySet(), lookupPath,request );
            String realLookupPath = requestURI.replaceAll(realLookupPathRegex, "$2");
            HandlerMethod handlerMethod = getMappingsByUrl(realLookupPath, requestVersion,request);
            if (handlerMethod != null){
                return handlerMethod;
            }
        } catch (Exception e) {
           log.info("request url ={},没有找到相应接口",lookupPath);
            e.printStackTrace();
        }
        //匹配失败
        return handleNoMatch(handlerMethods.keySet(), lookupPath,request );
    }

    private List<String> getDirectUrls(RequestMappingInfo mapping) {
        List<String> urls = new ArrayList<>();
        for (String path : getMappingPathPatterns(mapping)) {
            if (!getPathMatcher().isPattern(path)) {
                urls.add(path);
            }
        }
        return urls;
    }

    /**
     * 根据URL获取处理器方法
     * @param lookupPath
     * @param requestVersion
     * @param request
     * @return
     */
    private HandlerMethod getMappingsByUrl(String lookupPath,String requestVersion,HttpServletRequest request) {
        requestVersion = getVersion(requestVersion);
        //需要排序
        Map<String, RequestMappingInfo> mappingInfoMap = urlLookup.get(lookupPath);
        for (Map.Entry<String,RequestMappingInfo> mappingInfo : mappingInfoMap.entrySet()){
            String controllerVersion = mappingInfo.getKey();
            RequestMappingInfo mappingInfoValue = mappingInfo.getValue();
            if(controllerVersion.compareTo(requestVersion) <= 0){
                //修改请求路径
                Set<String> patterns = mappingInfoValue.getPatternsCondition().getPatterns();
                String newUrl = patterns.stream().findFirst().get();
//                request.setAttribute(HandlerMapping.LOOKUP_PATH,newUrl);
                //重新封装RequestMappingInfo
                RequestMappingInfo requestMappingInfo = getNewMappingInfo(mappingInfoValue);
                //校验requestMapping其他信息
                RequestMappingInfo matchingCondition = requestMappingInfo.getMatchingCondition(request);
                if(matchingCondition == null){
                    return null;
                }
                HandlerMethod handlerMethod = handlerMethods.get(mappingInfoValue);
                request.setAttribute(BEST_MATCHING_HANDLER_ATTRIBUTE, handlerMethod);
                handleMatch(matchingCondition, newUrl, request);
                return handlerMethod;
            }
        }
        return null;
    }

    private RequestMappingInfo getNewMappingInfo(RequestMappingInfo mappingInfoValue) {
        //修改请求路径
        return  new RequestMappingInfo(mappingInfoValue.getName(),
                new PatternsRequestCondition(),
                mappingInfoValue.getMethodsCondition(), mappingInfoValue.getParamsCondition(),
                mappingInfoValue.getHeadersCondition(),
                mappingInfoValue.getConsumesCondition(),
                mappingInfoValue.getProducesCondition(),
                mappingInfoValue.getCustomCondition());
    }

    /**
     * 获取版本号
     * @param input
     * @return
     */
    private  String getVersion(String input) {
        Pattern pattern = Pattern.compile(versionRegex);
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            return matcher.group(1); // 获取第一个捕获组的内容，即版本信息
        }
        return null;
    }


}
