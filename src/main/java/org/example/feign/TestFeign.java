package org.example.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author cai ping
 * @description:
 * @date 2024-12-12
 */
@FeignClient(name = "config")
@Component
public interface TestFeign {

    @GetMapping("/test")
    String test();

}
