package org.example.redissiontest;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author cai ping
 * @description:
 * @date 2024-11-25
 */
@RestController
@RequestMapping("/lock")
@Slf4j
public class TestLockController {


    @Resource
    private RedissonClient redissonClient;

    private RLock lock;

    @PostConstruct
    public void init() {
        lock = redissonClient.getLock("test-lock");
    }

    @GetMapping("/test")
    public void test() {
        for (int i = 0; i < 1; i++) {
           new Thread(() -> {

               long id = Thread.currentThread().getId();
               try {

                   lock.lock();
                   log.info("{}获得锁，执行业务代码", id);
                   log.info("业务代码!业务代码!业务代码!业务代码!业务代码!业务代码!业务代码!业务代码!业务代码!");

               } catch (Exception e) {
                   throw new RuntimeException(e);
               } finally {
                   lock.unlock();
                   log.info("{}释放锁,业务代码执行结束", id);
               }

           }).start();
        }
    }

}
