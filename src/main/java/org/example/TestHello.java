package org.example;

import lombok.SneakyThrows;
import org.aopalliance.intercept.MethodInterceptor;
import org.redisson.RedissonLock;
import org.springframework.aop.TargetSource;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.target.SingletonTargetSource;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-22
 */
public class TestHello {

    private static final ConcurrentMap<String, RedissonLock.ExpirationEntry> EXPIRATION_RENEWAL_MAP = new ConcurrentHashMap<>();

    @SneakyThrows
    public static void main(String[] args) {

//        List<VirtualMachineDescriptor> list = VirtualMachine.list();
//        for (VirtualMachineDescriptor virtualMachine : list) {
//            System.out.println(virtualMachine.id() + " " + virtualMachine.displayName());
//        }
//
//        Scanner scanner = new Scanner(System.in);
//        String s = scanner.nextLine();
//
//        VirtualMachine attach = VirtualMachine.attach(s);
//        attach.loadAgent("D:\\code\\my\\test-config\\target\\test-config.jar");
//
//        attach.detach();


        String cacheKey = "cacheKey";
        new Thread(() -> {
            long id = Thread.currentThread().getId();
            System.out.println("id = " + id);
            RedissonLock.ExpirationEntry expirationEntry = new RedissonLock.ExpirationEntry();
            expirationEntry.addThreadId(id);
            EXPIRATION_RENEWAL_MAP.put(cacheKey,expirationEntry);
        }).start();
        TimeUnit.SECONDS.sleep(1);
        new Thread(() -> {
            System.out.println("EXPIRATION_RENEWAL_MAP.get(cacheKey).getFirstThreadId() = " + EXPIRATION_RENEWAL_MAP.get(cacheKey).getFirstThreadId());
        }).start();




    }

    private static void extracted() {
        TargetSource simpleBeanTargetSource = new SingletonTargetSource(new Test());

        ProxyFactory proxyFactory = new ProxyFactory();

        proxyFactory.addAdvice(0, (MethodInterceptor) invocation -> {
            System.out.println("被拦截了！！！");

            return invocation.proceed();
        });
        proxyFactory.setTargetSource(simpleBeanTargetSource);


        Test proxy = (Test) proxyFactory.getProxy();

        proxy.sayHaha();
//        Tester tester = new Tester();
//        tester.testSoftware();
    }
}
