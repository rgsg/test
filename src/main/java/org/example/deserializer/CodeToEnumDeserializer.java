package org.example.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.example.enums.RatedWaterEnum;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author cai ping
 * @description:
 * @date 2023-11-27 11:34:55
 */
public class CodeToEnumDeserializer extends JsonDeserializer<RatedWaterEnum> {

    @Override
    public RatedWaterEnum deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        System.out.println("----------------------------------------------");
        int intValue = jsonParser.getIntValue();

        JsonStreamContext parsingContext = jsonParser.getParsingContext();

        String currentName = parsingContext.getCurrentName();
        Object currentValue = parsingContext.getCurrentValue();

        try{

            //获取枚举参数名称
            Field declaredField = currentValue.getClass().getDeclaredField(currentName);
            //获取对应的枚举类型
            Class<?> targetType  = declaredField.getType();
            Method valuesMethod  = targetType.getDeclaredMethod("values");
            RatedWaterEnum[] enums = (RatedWaterEnum[]) valuesMethod.invoke(null);
            //匹配code
            for (RatedWaterEnum ratedWaterEnum : enums){
                if(ratedWaterEnum.getStatus() == intValue){
                    return ratedWaterEnum;
                }
            }
            throw new RuntimeException("不匹配");
        }catch (Exception e){
            throw new RuntimeException("不匹配",e);
        }

    }
}
