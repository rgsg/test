package org.example.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import org.example.enums.TestEnum;

import java.io.IOException;

/**
 * BaseEnum 序列化
 */
@JacksonStdImpl
public class BaseEnumDeSerializer extends JsonDeserializer<TestEnum> {


    @Override
    public TestEnum deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String value = p.getText();
        TestEnum[] values = TestEnum.values();
        for (TestEnum testEnum : values){
            if(testEnum.getCode().equals(value)){
                return testEnum;
            }
        }
        return null;
    }
}
