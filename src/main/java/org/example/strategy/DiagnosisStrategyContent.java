package org.example.strategy;

import com.google.common.collect.Maps;
import org.example.strategy.impl.DiagnosisDeviceStrategyImpl;
import org.example.strategy.impl.DiagnosisOrgStrategyImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author cai ping
 * @description:
 * @date 2024-01-12
 */
@Service
public class DiagnosisStrategyContent {

    private Map<String,DiagnosisStrategy> map;
    public DiagnosisStrategyContent(){
        map = Maps.newHashMap();
        map.put("0",new DiagnosisOrgStrategyImpl());
        map.put("1",new DiagnosisDeviceStrategyImpl());

    }

    public void process(String code){
        map.get(code).process();
    }

}
