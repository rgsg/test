package org.example.strategy.impl;

import org.example.strategy.DiagnosisStrategy;

/**
 * @author cai ping
 * @description:
 * @date 2024-01-12
 */
public class DiagnosisDeviceStrategyImpl implements DiagnosisStrategy {
    @Override
    public void process() {
        System.out.println("---------------------DiagnosisDeviceStrategyImpl-----------------------------");
    }
}
