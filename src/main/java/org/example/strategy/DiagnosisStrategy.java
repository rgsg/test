package org.example.strategy;

/**
 * @author cai ping
 * @description:
 * @date 2024-01-11
 */
public interface DiagnosisStrategy {

    void process();
}
