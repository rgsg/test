package org.example.parsers;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.parser.NodeParser;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;

import java.util.Map;

/**
 * @author cai ping
 * @description:
 * @date 2024-03-08
 */
public class TreeNodeParser<T,E> implements NodeParser<T, E> {

    @Override
    public void parse(T object, Tree<E> treeNode) {
        Map<String,Object> javaObject = JSONObject.parseObject(JSONObject.toJSONString(object),new TypeReference<Map<String, Object>>(){});
        javaObject.forEach(treeNode::putExtra);
    }
}
