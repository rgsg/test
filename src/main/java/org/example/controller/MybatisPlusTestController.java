package org.example.controller;

import org.example.entity.Area;
import org.example.service.AreaService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author cai ping
 * @description:
 * @date 2024-04-26
 */
@RestController
public class MybatisPlusTestController {

    @Resource
    AreaService areaService;



    /**
     *
     * 单条语句操作： 每次都从datasource取连接，并且是自动提交的事务
     * 批处理操作： 如果是同一个sql，同一个statement那么就是一个连接，底层也是jdbc批处理，不是自动提交事务
     * 加@Transaction注解，所有操作都是非自动提交的，再提交之前判断是否是同步的sqlSession，如果从线程上下文取到了sqlSessionHolder则是，最后由手动事务提交
     * @return
     */

    @Resource
    AreaService AreaService;

    @PostMapping(value = "/mp-insert")
    public List<Area> insert(@RequestBody Area area) {
//        System.out.println(area);
//        List<Area> list = areaService.lambdaQuery().eq(Area::getName, "测试啊").list();
//        return list;
        return null;
    }


}
