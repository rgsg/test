package org.example.controller;

//import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;
//import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import lombok.SneakyThrows;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-10
 */
@RestController
@RequestMapping(value = "/pdf")
public class PDFTestController {

    @GetMapping("/test")
    @SneakyThrows
    public void test(HttpServletResponse response){
        URL url = new URL("http://gsecurity.test.eslink.net.cn:32080/gs/api/v1/admin-api/infra/file/20/get/5c8e5dddc145079a84cd6896ddd1b5ce33edd52c31c571ac6c85aa528d9c2fc2.docx");
        URLConnection urlConnection = url.openConnection();
        InputStream inputStream = urlConnection.getInputStream();
        XWPFDocument xwpfDocument = new XWPFDocument(inputStream);
//        PdfOptions pdfOptions = PdfOptions.create();
//        PdfConverter.getInstance().convert(xwpfDocument,response.getOutputStream(),pdfOptions);
        inputStream.close();
    }
    @GetMapping("/test2")
    @SneakyThrows
    public void test2(HttpServletResponse response){
//        URL url = new URL("http://gsecurity.test.eslink.net.cn:32080/gs/api/v1/admin-api/infra/file/20/get/5c8e5dddc145079a84cd6896ddd1b5ce33edd52c31c571ac6c85aa528d9c2fc2.docx");
//        URLConnection urlConnection = url.openConnection();
        FileInputStream fileInputStream = new FileInputStream("C:\\Users\\G012255\\Documents\\WXWork\\1688855079532635\\Cache\\File\\2024-05\\RD01-用户真实数据使用申请操作指引.doc");
//        InputStream inputStream = urlConnection.getInputStream();
    }
}
