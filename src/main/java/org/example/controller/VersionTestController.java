package org.example.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cai ping
 * @description:
 * @date 2024-03-13
 */
@RestController
@RequestMapping("/api/app")
public class VersionTestController {

    @GetMapping(value = "/v1/test")
    public String testV1(){

        return "/test/v1";
    }


    @GetMapping(value = "/v2/test")
    public String testV2(){
        return "/test/v2";
    }

    @GetMapping(value = "/test")
    public String test3(){
        return "test";
    }
}
