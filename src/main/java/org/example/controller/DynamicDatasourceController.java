package org.example.controller;

import javassist.ClassPool;
import javassist.CtClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cai ping
 * @description:
 * @date 2024-11-01
 */
@RequestMapping("/dynamic-datasource")
@RestController
@Slf4j
public class DynamicDatasourceController {

    @GetMapping("/insert")
    public void insert() throws Exception {
//        throw new RuntimeException("系统异常");
        ClassPool aDefault = ClassPool.getDefault();
        CtClass ctClass = aDefault.get("org.example.TestHello");
        System.out.println(ctClass);
    }

}
