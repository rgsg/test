package org.example.controller;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.apache.ibatis.cursor.Cursor;
import org.example.api.BootHelloGrpc;
import org.example.api.Hello;
import org.example.api.HelloServiceGrpc;
import org.example.api.TestBootHello;
import org.example.entity.Customers;
import org.example.entity.Dict;
import org.example.entity.Student;
import org.example.entity.TestTime;
import org.example.feign.TestFeign;
import org.example.mapper.kaifa.MtEquipmentMapper;
import org.example.mapper.mysql1.CustomersMapper;
import org.example.mapper.mysql1.DictMapper;
import org.example.mapper.mysql1.UserMapper;
import org.example.service.CacheTest;
import org.example.service.TestService;
import org.example.service.UserService;
import org.example.ws.WebSocketServer;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author cai ping
 * @description: body ----> RequestResponseBodyMethodProcessor
 * get url参数、post表单提交  ----> ServletModelAttributeMethodProcessor
 * @date 2023-11-24 15:11:22
 */
@RestController
@SuppressWarnings("all")
@Slf4j
public class TestController {
    @Resource
    UserMapper userMapper;

    @Resource
    UserService userService;


    @GetMapping("/testMultiThreadInsert")
    public void testMultiThreadInsert() throws ExecutionException, InterruptedException {

        userService.batchInsert();

    }

    @GetMapping("testWhile")
    public void testWhile() {
        int i = 0;
        /**
         * redis 线程模型，是一个基于事件监听的io多路复用模型，io多路复用指的是用一个线程处理多个网络io请求，而不需要针对每个网络io都分配一个处理线程，
         * 他的复用机制基于select或epoll来实现，select返回一个有限的文件描述符集合，用户态需要去遍历发生了事件的文件描述符再处理请求，而epoll是一个基于事件驱动的模型，
         * 当文件描述符变化时，会直接通知用户态进行操作，不需要再去遍历fd，适合大规模的网络io操作。
         *
         * redis过期删除策略，惰性删除和定期删除，定期删除是值以一定的频率抽取一部分key来判断是否过期，如果过期则删除，惰性删除是值，当key被访问时，去判断是否过期，如果过期了则删除。
         *
         * redis持久化机制，rdb和aof，rdb是一个二进制持久化快照文件，根据其配置的阈值，15分钟，9分钟，1分钟的key变化数量来进行持久化，通过save或者bgsave创建一个子进程完成临时文件
         * 持久化，持久化完成后替换掉原有的rdb文件，文件空间紧密一般持久化文件较小，aof是一个日志持久化文件，他同样以一定的频率完成持久化操作，always，everysec，no分别对应每次key变化
         * 都去同步到硬盘，和每秒同步一次，no表示不持久化，写入到内核缓存区，由操作系统完成fsync的操作。aof文件相比rdb数据完整性更高，因为每次都是追加写入以日志的方式，所以可读性高
         * 文件会比较大，数据恢复会比较慢。
         */
        while (true) {
            i++;
        }

    }

    @GetMapping("/convert")
    public void convert(String url, HttpServletResponse response) {
        ByteArrayOutputStream outputStream = null;
        try {
            String decodeUrl = URLDecoder.decode(url, "utf-8");
            outputStream = new ByteArrayOutputStream();
//            WordToPdfUtil.word2Pdf2(decodeUrl, outputStream);
            RestTemplate restTemplate = new RestTemplate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException ignored) {
                }
            }
        }
    }


    @PostMapping("/date")
    public TestTime date(TestTime testTime) {
        System.out.println(testTime);
        return testTime;
    }

    @Resource
    private DataSourceTransactionManager transactionManager;

    @GetMapping("insert")
    public void insert() {
        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
        transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus transactionStatus = transactionManager.getTransaction(transactionDefinition);
        try {
            for (int i = 0; i < 100; i++) {
            }
            transactionManager.commit(transactionStatus);
        } catch (Exception e) {
            transactionManager.rollback(transactionStatus);
        }
    }

    @GetMapping("/testHolder")
    public void testHolder() {
        System.out.println(Thread.currentThread().getName());
    }

    @Resource
    private DictMapper dictMapper;

    @GetMapping("/list")
    public Map<String, List<Dict>> list() {
//        LambdaQueryWrapper<Dict> wrapper = new LambdaQueryWrapper<>();
//        wrapper.eq(Dict::getStatus, 1);
//        List<Dict> list = dictMapper.selectList(wrapper);
//        Map<String, List<Dict>> codeMap = list.stream().collect(Collectors.groupingBy(Dict::getSortCode));
//        return codeMap;
        return null;
    }

    /**
     * mybatis-plus多数据源的处理
     * 1. 通过MapperScan扫描指定包下的Mapper接口。
     * 2. 将Mapper接口解析成MapperFactoryBean，并绑定3个属性，mapperInterface，addToConfig，SqlSessionFactory(关键)
     * 3. spring创建mapper bean进行代理
     * 4. 调用Mapper接口执行，通过sqlSessionFactory创建session实现多数据源
     */

    @Resource
    private CacheTest cacheTest;

    @GetMapping("/getParameterName")
    public String getParameterName() {
        Method dia;
        try {
            dia = TestController.class.getMethod("tree", List.class);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
        Parameter[] parameters = dia.getParameters();
        System.out.println(parameters[0].getName());
        return parameters[0].getName();
    }

    @GetMapping("/cache")
//    @Cacheable(cacheNames = "test",key = "'data'")
//    @MyCache(cacheNames = "mycache",key = "'data'")
    public String cache() {
        List<String> s = cacheTest.cacheTest();
        return null;
    }

    @GetMapping("/cache2")
    public Object cache2() {

        return cacheTest.cacheTest2();
    }

    @Resource
    TestService testService;

    @Resource
    private TestFeign testFeign;

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private WebSocketServer webSocketServer;

    @GetMapping("/testt")
    public Student testt() {
        log.info("调用请求,{}", "test start");
        ResponseEntity<String> forEntity = restTemplate.getForEntity("http://localhost:8083/test", String.class);
//        testFeign.test();
        log.info("调用请求,{}", "test end");
//        return forEntity.getBody();
        Student student = new Student();
        student.setName("caiping");
        student.setAge("11");

        return student;
    }

    @GetMapping("/test2")
    public void test2() {
        userService.testt();
    }


    @Resource
    private CustomersMapper customersMapper;

    /**
     * 全量180m
     * 分批 150m 94s
     *
     * @param response
     */

    @Resource
    private MtEquipmentMapper equipmentMapper;

    @GetMapping("/export")
    @SneakyThrows
    @Transactional
    public void export(HttpServletResponse response) {

        // 创建一个线程池，大小为5
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        long start = System.currentTimeMillis();
        printMemoryUsage();

        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode("测试导出", "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

        // 创建ExcelWriter并在流中写入
        try (
                ExcelWriter build = EasyExcel.write(response.getOutputStream(), Customers.class).build();
        ) {
            // 获取数据库数据
            try (
                    Cursor<Customers> customers = customersMapper.selectCursor();
            ) {
                AtomicInteger atomicInteger = new AtomicInteger(0);

                // 创建一个list存储每个异步任务
                List<CompletableFuture<Void>> futures = new ArrayList<>();

                // 启动多个线程，每个线程写一个sheet
                for (int i = 0; i < 5; i++) {
                    CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                        List<Customers> data = new ArrayList<>();
                        WriteSheet writerSheet = EasyExcel.writerSheet("数据" + atomicInteger.incrementAndGet()).build();

                        // 遍历客户数据，每个线程处理一定的数据量
                        for (Customers customer : customers) {
                            data.add(customer);
                            // 每10000条数据批量写入一次
                            if (data.size() == 10000) {
                                synchronized (build) {
                                    // 写入到指定的sheet
                                    build.write(data, writerSheet);
                                }
                                data.clear();
                            }
                        }
                        // 如果剩余数据少于10000条，写入剩余的数据
                        if (!data.isEmpty()) {
                            synchronized (build) {
                                build.write(data, writerSheet);
                            }
                        }
                    }, executorService);

                    futures.add(future);
                }

                // 等待所有线程执行完毕
                CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            // 关闭线程池
            executorService.shutdown();
        }

        long endTime = System.currentTimeMillis();
        System.out.println("耗时: " + (endTime - start));
        System.out.println("----------------------------------------------------------------------------");
        printMemoryUsage();
    }

    @GetMapping("/cursor")
    @SneakyThrows
    @Transactional
    public void cursor() {

        Cursor<Customers> customers = customersMapper.selectCursor();

        Iterator<Customers> iterator = customers.iterator();
        while (iterator.hasNext()) {
            Customers next = iterator.next();
        }

    }

    @GrpcClient(value = "grpc-service")
    BootHelloGrpc.BootHelloBlockingStub blockingStub;

    @GrpcClient(value = "grpc-service")
    HelloServiceGrpc.HelloServiceBlockingStub helloServiceStub;

    @GetMapping("/testRpc")
    @SneakyThrows
    public String sendMsg(String name) {


        TestBootHello.BootResp resp = blockingStub.test(TestBootHello.BootReq.newBuilder().setName(name).build());

        log.info("test.getResult() = {}", resp.getResult());

        Hello.HelloResp hello = helloServiceStub.hello(Hello.HelloReq.newBuilder().setName("wangwu").build());
        log.info("hello.getResult() = {}", hello.getResult());

        return resp.getResult();

    }

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @GetMapping("/redis")
    public void testRedis() throws ParseException {

        String wkt = "POLYGON ((120.062717 30.502219,119.628757 30.519966,119.564212 30.233253,119.902042 30.19053,120.062717 30.502219))";
        WKTReader reader = new WKTReader();
        Geometry geometry = reader.read(wkt);

        String wkt2 = "POLYGON ((120.864981 30.313902,120.173953 30.08364,120.793994 30.127598,121.098865 30.549537,120.48569 30.654737,120.864981 30.313902))";
        Geometry geometry1 = reader.read(wkt2);
        boolean intersects = geometry.intersects(geometry1);
        if (intersects) {
            System.out.println("区域相交");
        } else {
            System.out.println("区域不相交");
        }
    }
    /**
     * [[120.062717,30.502219],[119.628757,30.519966],[119.564212,30.233253],[119.902042,30.19053]]
     * [[120.864981,30.313902],[120.173953,30.08364],[120.793994,30.127598],[121.098865,30.549537],[120.48569,30.654737]]
     */

    /**
     * wkt在线查看
     * https://clydedacruz.github.io/openstreetmap-wkt-playground/
     * GEOMETRYCOLLECTION(POLYGON ((120.062717 30.502219,119.628757 30.519966,119.564212 30.233253,119.902042 30.19053,120.062717 30.502219)),POLYGON ((120.864981 30.313902,120.173953 30.08364,120.793994 30.127598,121.098865 30.549537,120.48569 30.654737,120.864981 30.313902)))
     */

    @GetMapping("/put")
    public void put(String userId) {


        redisTemplate.opsForValue().setBit("day", 1, true);

        Boolean day = redisTemplate.opsForValue().getBit("day", 1);
        System.out.println("day = " + day);

    }


    public static void printMemoryUsage() {
        Runtime runtime = Runtime.getRuntime();
        long totalMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        long usedMemory = totalMemory - freeMemory;
        System.out.println("Total Memory: " + totalMemory / 1024 / 1024 + " MB");
        System.out.println("Free Memory: " + freeMemory / 1024 / 1024 + " MB");
        System.out.println("Used Memory: " + usedMemory / 1024 / 1024 + " MB");
    }

}
