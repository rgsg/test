package org.example.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cai ping
 * @description:
 * @date 2024-06-04
 */

/**  bootstrap远程配置 > 系统属性property > 系统环境变量 > 外层配置  > 内层 > bootstrap.yml <br/>
 *  总结: <br/>
 *      springboot配置文件加载规则:<br/>
 *          1. 通过监听EnvironmentPreparedEvent 创建一个Loader加载property和yaml文件<br/>
 *          2. 一般情况下profiles会放入两个  一个null用于加载普通的配置文件  一个defaultProfile用于加载特征文件<br/>
 *          3. 默认加载规则:<br/>
 *              location: ./config/ > ./ > classpath:/config > classpath:/<br/>
 *              properties > xml > yml > yaml<br/>
 *          4. 由于特征文件相关后加载，因此对加载的列表进行反转<br/>
 *      springCloud配置加载Bootstrap文件<br/>
 *          1. 通过监听EnvironmentPreparedEvent 创建一个容器加载bootstrap文件<br/>
 *          2. 通过PropertySourceBootstrapConfiguration 加载注册中心配置 如zk，Nacos 最后通过实现PropertySourceLocator完成远程加载<br/>
 *
 *              addFirst添加 因此越具体优先级越高 特征 > 普通 > ext > shared
 *            nacos加载:<br/>
 *             this.loadSharedConfiguration(composite); 加载共享<br/>
 *             this.loadExtConfiguration(composite); 加载扩展<br/>
 *             this.loadApplicationConfiguration(composite, dataIdPrefix, this.nacosConfigProperties, env); 加载普通<br/>
 *
 */
@RestController
@RequestMapping("/refresh")
@Slf4j
public class RefreshController {

//    @Resource
//    RefreshService refreshService;
//    @Resource
//    TestHello testHello;
    /**
     * nacos 应用配置会加载3次
     * 1. dataId
     * 2. dataId.扩展名
     * 3. dataId-activeProfile.扩展名
     * file:./config/ file:./config/* file:./ classpath:/config/ classpath:/
     */
    @GetMapping("/test")
    public void test(){
//        System.out.println(refreshService.getTopics());
    }
}
