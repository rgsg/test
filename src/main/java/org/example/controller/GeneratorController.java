package org.example.controller;

import org.example.service.DocumentSqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description 生成数据库文档
 * @Author YeHaoNan~
 * @Date 2023-02-15 21:42
 * @Version 1.0.0
 **/
@RestController
@RequestMapping("/generator")
public class GeneratorController {

    private final DocumentSqlService documentSqlService;

    @Autowired
    public GeneratorController(DocumentSqlService documentSqlService) {
        this.documentSqlService = documentSqlService;
    }

    /**
     * 生成数据库表结构文档
     */
    @RequestMapping("/generatorDocument")
    public void generatorDocument(){
        documentSqlService.generatorDocument();
    }
}

