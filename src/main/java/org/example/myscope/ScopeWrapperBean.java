package org.example.myscope;

import org.springframework.beans.factory.ObjectFactory;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-17
 */
public class ScopeWrapperBean {

    private Object bean;

    private Runnable callback;

    private final String name;

    private final ObjectFactory<?> objectFactory;

    public ScopeWrapperBean(String name, ObjectFactory<?> objectFactory) {
        this.name = name;
        this.objectFactory = objectFactory;
    }

    public Object getBean() {
        if(bean == null){
            synchronized (this.name) {
                if (this.bean == null) {
                    this.bean = this.objectFactory.getObject();
                }
            }
        }
        return bean;
    }
}
