package org.example.myscope;

import org.springframework.aop.scope.ScopedProxyFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.Scope;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.cloud.context.scope.GenericScope;
import org.springframework.cloud.context.scope.StandardScopeCache;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-17
 */
@Component
public class MyRefreshScopeHandler implements Scope, BeanFactoryPostProcessor, BeanDefinitionRegistryPostProcessor {

    StandardScopeCache cache = new StandardScopeCache();

    private String name = "my-refresh";


    @Override
    public Object get(String name, ObjectFactory<?> objectFactory) {
        ScopeWrapperBean value = (ScopeWrapperBean) cache.put(name, new ScopeWrapperBean(name, objectFactory));

        return value.getBean();
    }

    @Override
    public Object remove(String name) {
        return cache.remove(name);
    }

    @Override
    public void registerDestructionCallback(@NotNull String name, Runnable callback) {

    }

    @Override
    public Object resolveContextualObject(String key) {
        return null;
    }

    @Override
    public String getConversationId() {
        return null;
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        beanFactory.registerScope(this.name, this);
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        for (String beanDefinitionName : registry.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = registry.getBeanDefinition(beanDefinitionName);
            if (beanDefinition instanceof RootBeanDefinition) {
                RootBeanDefinition root = (RootBeanDefinition) beanDefinition;
                if (root.getDecoratedDefinition() != null && root.hasBeanClass()
                        && root.getBeanClass() == ScopedProxyFactoryBean.class) {
                    if (this.name.equals(root.getDecoratedDefinition().getBeanDefinition()
                            .getScope())) {
                        root.setBeanClass(GenericScope.LockedScopedProxyFactoryBean.class);
                    }
                }
            }
        }
    }
}
