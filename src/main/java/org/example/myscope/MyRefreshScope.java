package org.example.myscope;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import java.lang.annotation.*;

@Target({ElementType.TYPE,ElementType.METHOD})
@Scope("my-refresh")
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyRefreshScope {

    ScopedProxyMode proxyMode() default ScopedProxyMode.TARGET_CLASS;

}
