package org.example.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.annotation.MyCache;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
public class CustomizedRedisCacheManager extends RedisCacheManager implements BeanPostProcessor, Ordered {

    private final Map<Object, RedisCacheConfiguration> attributeCache = new ConcurrentHashMap<>(1024);

    private final RedisCacheWriter cacheWriter;
    private final RedisCacheConfiguration defaultCacheConfig;
    private final Map<String, RedisCacheConfiguration> initialCacheConfiguration;
    private final boolean allowInFlightCacheCreation;


    public CustomizedRedisCacheManager(RedisCacheWriter cacheWriter, RedisCacheConfiguration defaultCacheConfiguration,
                             Map<String, RedisCacheConfiguration> initialCacheConfigurations) {
        super(cacheWriter,defaultCacheConfiguration,initialCacheConfigurations);
        this.cacheWriter = cacheWriter;

        this.defaultCacheConfig = defaultCacheConfiguration;
        this.initialCacheConfiguration = initialCacheConfigurations;

        this.allowInFlightCacheCreation = true;

    }

    @Override
    public RedisCache createRedisCache(String name, @Nullable RedisCacheConfiguration cacheConfig) {
        //此处映射缓存配置
        RedisCacheConfiguration redisCacheConfiguration = attributeCache.get(name);
       return super.createRedisCache(name,  redisCacheConfiguration != null ? redisCacheConfiguration : cacheConfig);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Method[] declaredMethods = bean.getClass().getDeclaredMethods();
        for (Method method : declaredMethods) {
            MyCache annotation = method.getAnnotation(MyCache.class);
            if(Objects.nonNull(annotation)) {
                String[] cacheNames = annotation.cacheNames();
                int expire = annotation.expire();
                TimeUnit unit = annotation.unit();
                RedisCacheConfiguration redisCacheConfigurationWithTtl = getRedisCacheConfigurationWithTtl(expire, unit);
                for (String cacheName : cacheNames) {
                    attributeCache.put(cacheName,redisCacheConfigurationWithTtl);
                }
            }
        }

        return bean;
    }

    @SuppressWarnings("all")
    private RedisCacheConfiguration getRedisCacheConfigurationWithTtl(Integer seconds,TimeUnit timeUnit) {
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig();
        redisCacheConfiguration = redisCacheConfiguration.serializeValuesWith(
                RedisSerializationContext
                        .SerializationPair
                        .fromSerializer(jackson2JsonRedisSerializer)
        ).entryTtl(Duration.of(seconds, ChronoUnit.SECONDS));
        return redisCacheConfiguration;
    }

    @Override
    public int getOrder() {
        return Integer.MIN_VALUE;
    }
}