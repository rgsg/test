package org.example.config;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Configuration;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-22
 */
@Configuration
public class MyAdvice {


//    @Bean
    public Advisor advice(){
        AspectJExpressionPointcut aspectJExpressionPointcut = new AspectJExpressionPointcut();
        aspectJExpressionPointcut.setExpression("execution(* sayHello())");
        DefaultPointcutAdvisor defaultPointcutAdvisor1 = new DefaultPointcutAdvisor();
        DefaultPointcutAdvisor defaultPointcutAdvisor = new DefaultPointcutAdvisor(new MethodInterceptor() {
            @Override
            public Object invoke(MethodInvocation invocation) throws Throwable {
                Object proceed = (Object) invocation.proceed();
                return  proceed;
            }
        });

        return defaultPointcutAdvisor;
    }

}
