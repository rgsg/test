package org.example.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
import org.springframework.context.annotation.Configuration;

/**
 * @author cai ping
 * @description:
 * @date 2024-09-02
 */
@ConditionalOnSingleCandidate(XxlJobConfig.class)
@Configuration
public class TestConfig {

    public TestConfig() {
        System.out.println("---------------TestConfig被加载了---------------");
    }
}
