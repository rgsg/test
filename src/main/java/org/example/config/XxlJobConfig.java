package org.example.config;

import com.xxl.job.core.executor.XxlJobExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * xxl-job config
 *
 * @author xuxueli 2017-04-28
 */
@Configuration
@ConfigurationProperties(prefix = "xxl.job")
public class XxlJobConfig {
    private Logger logger = LoggerFactory.getLogger(XxlJobConfig.class);
    private String addresses;

    private String accessToken;

    private String appname;

    private String address;

    private String ip;

    private int port;

    private String logPath;

    private int logRetentionDays;

    public XxlJobConfig() {
    }

    @Bean(
            initMethod = "start",
            destroyMethod = "destroy"
    )
    public XxlJobExecutor xxlJobExecutor() {

        this.logger.info("xxl-job init start. adminAddresses: {} appName: {} port: {} logPath:{}", this.addresses, this.appname, this.port, this.logPath);
        XxlJobExecutor xxlJobExecutor = new XxlJobExecutor();
        xxlJobExecutor.setAdminAddresses(this.addresses);
        xxlJobExecutor.setAppname(this.appname);
        xxlJobExecutor.setPort(this.port);
        xxlJobExecutor.setIp(ip);
        xxlJobExecutor.setLogPath(this.logPath);
        xxlJobExecutor.setAccessToken(this.accessToken);
        xxlJobExecutor.setLogRetentionDays(this.logRetentionDays);
        return xxlJobExecutor;
    }

    /**
     *
     *  针对多网卡、容器内部署等情况，可借助 "spring-cloud-commons" 提供的 "InetUtils" 组件灵活定制注册IP；
     *
     *      1、引入依赖：
     *          <dependency>
     *             <groupId>org.springframework.cloud</groupId>
     *             <artifactId>spring-cloud-commons</artifactId>
     *             <version>${version}</version>
     *         </dependency>
     *
     *      2、配置文件，或者容器启动变量
     *          spring.cloud.inetutils.preferred-networks: 'xxx.xxx.xxx.'
     *
     *      3、获取IP
     *          String ip_ = inetUtils.findFirstNonLoopbackHostInfo().getIpAddress();
     */


}