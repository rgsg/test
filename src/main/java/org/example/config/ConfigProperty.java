package org.example.config;

import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-09
 */
@ConfigurationProperties(prefix = "config.test")
@Configuration
@ToString
public class ConfigProperty {

    private String name;

    private String age;

    public void setAge(String age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

//    @Bean
    public XxlJobConfig xxlJobConfig2() {
        return new XxlJobConfig();
    }
}
