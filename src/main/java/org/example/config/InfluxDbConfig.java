package org.example.config;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cai ping
 * @description:
 * @date 2023-11-29 9:05:15
 */
//@ConfigurationProperties(prefix = "influxdb")
public class InfluxDbConfig {
    Map<String,String> database = new HashMap<>();

    String topic;

    public void setDatabase(Map<String, String> database) {
//        System.out.println("------------");
        this.database = database;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}
