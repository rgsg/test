package org.example.utils;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import org.example.parsers.TreeNodeParser;

import java.util.List;

/**
 * @author cai ping
 * @description:
 * @date 2024-03-08
 */
public class TreeNodeUtil {

    /**
     *
     * @param list 需要构建树形结构的列表
     * @return List<Tree<Integer>> 返回树形列表
     * @param <T>
     */

    public static <T> List<Tree<Integer>> getTree(List<T> list){
        return getTreeByParentId(list,0,TreeNodeConfig.DEFAULT_CONFIG);
    }

    public static <T> List<Tree<Integer>> getTree(List<T> list,TreeNodeConfig config){
        return getTreeByParentId(list,0,config);
    }

    /**
     *
     * @param list
     * @param parentId 根节点的父id，一般为0
     * @return
     * @param <T>
     * @param <E>
     */
    public static <T,E> List<Tree<E>> getTreeByParentId(List<T> list, E parentId, TreeNodeConfig config){
        return TreeUtil.build(list,parentId,config,new TreeNodeParser<T,E>());
    }
}
