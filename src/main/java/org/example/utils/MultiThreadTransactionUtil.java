package org.example.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author cai ping
 * @description:
 * @date 2025-01-08
 */
@Slf4j
public class MultiThreadTransactionUtil {

    public static void run(List<Runnable> works, PlatformTransactionManager transactionManager) {

        if (works.isEmpty()) return;

        AtomicBoolean exceptionFlag = new AtomicBoolean(false);

        CountDownLatch countDownLatch = new CountDownLatch(works.size());

        CompletableFuture[] completableFutures = new CompletableFuture[works.size()];

        for (int i = 0; i < works.size(); i++) {

            Runnable work = works.get(i);

            CompletableFuture<Void> future = CompletableFuture.runAsync(() ->
            {
                TransactionStatus transactionStatus = null;
                try {
                    transactionStatus = transactionManager.getTransaction(new DefaultTransactionDefinition());
                    work.run();
                } catch (Exception e) {
                    exceptionFlag.set(true);
                    log.error("系统异常,msg={}", e.getMessage());
                } finally {
                    countDownLatch.countDown();
                    try {
                        countDownLatch.await();
                    } catch (Exception ignore) {
                    }finally {
                        if (transactionStatus != null) {
                            if (exceptionFlag.get()) {
                                log.info("rollback===");
                                transactionManager.rollback(transactionStatus);
                            } else {
                                log.info("commit===");
                                transactionManager.commit(transactionStatus);
                            }
                        }
                    }
                }
            });
            completableFutures[i] = future;
        }
        CompletableFuture.allOf(completableFutures).join();
        log.info("线程结束====");
    }

}
