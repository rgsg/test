package org.example.liftcycle;

import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

/**
 * @author cai ping
 * @description:
 * @date 2024-06-13
 */
@Component
public class TestLifeCycle implements SmartLifecycle {
    private boolean started = false;
    @Override
    public void start() {
            started = true;
            System.out.println("TestLifeCycle ----启动");
    }

    @Override
    public void stop() {
            started = false;
            System.out.println("TestLifeCycle ----关闭");
    }

    @Override
    public boolean isRunning() {
        return started;
    }
}
