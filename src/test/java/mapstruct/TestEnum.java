package mapstruct;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TestEnum {
    PASS("PASS", "通过"), UNPASS("UNPASS", "不通过"),

    MALE("MALE","男"),WOMEN("WOMEN","女");

    private String code;

    private String description;
}
