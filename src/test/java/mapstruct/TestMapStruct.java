package mapstruct;

import mapstruct.convert.StudentConvert;
import mapstruct.dto.BookRuleDTO;
import mapstruct.dto.StudentDto;
import mapstruct.vo.StudentVo;

/**
 * @author cai ping
 * @description:
 * @date 2024-06-25
 */

public class TestMapStruct {
    public static void main(String[] args) {

        StudentDto studentDto = new StudentDto();
        studentDto.setCreator("caiping");
        studentDto.setStudentName("乌鱼飞");
        BookRuleDTO bookRuleDTO = BookRuleDTO.builder().countMin("100").build();
        studentDto.setBookRuleDTO(bookRuleDTO);

        StudentVo studentVos = StudentConvert.INSTANCE.studentPoToVo(studentDto);
        System.out.println("studentVos = " + studentVos);
    }

}
