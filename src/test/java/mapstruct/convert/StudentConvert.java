package mapstruct.convert;

import cn.hutool.core.date.DateUtil;
import mapstruct.dto.StudentDto;
import mapstruct.vo.StudentVo;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(imports = {DateUtil.class})
@DecoratedWith(value = StudentConvertDecorator.class)
public interface StudentConvert {
    StudentConvert INSTANCE = Mappers.getMapper(StudentConvert.class);

    @Named("nameMap")
    default String nameMap(String studentName) {
        return studentName + "hhhhh";
    }


    @Named("toBean")
    default Object toBean(Object dto) {
        StudentVo studentVo = new StudentVo();
        studentVo.setAge("20");
        studentVo.setName("aaaaa");
        return studentVo;
    }

    //    @Named("studentPoToVo")
    StudentVo studentPoToVo(StudentDto dto);


//    void studentVoUpdate(StudentDto dto, @MappingTarget StudentVo vo);

    @InheritInverseConfiguration
    StudentDto studentVoToDto(StudentVo vo);

//    @IterableMapping(qualifiedByName = "studentPoToVo")
//    List<StudentVo> studentPoToListVo(List<StudentDto> dto, @Context Map<Object, Object> map);

    @AfterMapping
    default void after(StudentDto dto, @MappingTarget StudentVo vo) {
        System.out.println("-------after-------");
    }


}
