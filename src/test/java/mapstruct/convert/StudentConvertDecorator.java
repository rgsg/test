package mapstruct.convert;

import cn.hutool.core.date.DateUtil;
import mapstruct.dto.StudentDto;
import mapstruct.vo.StudentVo;

/**
 * @author cai ping
 * @description:
 * @date 2025-01-03
 */
public abstract class StudentConvertDecorator implements StudentConvert {

    private StudentConvert studentConvert;

    public StudentConvertDecorator(StudentConvert delegate) {

        this.studentConvert = delegate;
    }

    @Override
    public StudentDto studentVoToDto(StudentVo vo) {
        StudentDto studentDto = studentConvert.studentVoToDto(vo);
        studentDto.setCreateTime(DateUtil.date());
        return studentDto;
    }

}
