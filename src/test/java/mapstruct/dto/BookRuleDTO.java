package mapstruct.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BookRuleDTO {
 
    private Integer checkinMin;
    private Integer checkinMax;
    private String countMin;
    private String countMax;
 
}
