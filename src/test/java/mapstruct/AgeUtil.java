package mapstruct;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-05
 */
public class AgeUtil {

    public static String getDesc(String code){
        if(Integer.parseInt(code) > 20){
            return "成年";
        }
        return "未成年";
    }
}
