package mapstruct.vo;

import lombok.Data;

/**
 * @author cai ping
 * @description:
 * @date 2024-07-05
 */
@Data
public class BaseVo {

    private String creator;

    private String updater;

}
