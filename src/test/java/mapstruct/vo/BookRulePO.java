package mapstruct.vo;

import lombok.Data;

@Data
public class BookRulePO {
 
    private Integer checkinMin;
    private Integer checkinMax;
    private Integer roomCountMin;
    private Integer roomCountMax;
 
}