package mapstruct.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @author cai ping
 * @description:
 * @date 2024-06-25
 */
@Data
@ToString
public class StudentVo {

    private String name;

    private String age;

    private String description;

    private String creator;

    private BookRulePO bookRulePO;

}
