package fanxing;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GenericTest{


    public static <T,K,V> Map<K,V> listToMap(List<T> data, Function<T,K> kFunc, Function<T,V> vFunc) {

        return data.stream().collect(Collectors.toMap(kFunc,vFunc));

    }

    public static void main(String[] args) {


    }
}
