package redis;

import org.example.Main;
import org.example.controller.TestController;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author cai ping
 * @description:
 * @date 2024-05-11
 */
@SpringBootTest(classes = Main.class)
@RunWith(SpringRunner.class)
public class RedisTest {
    @Resource
    RedisTemplate<String,Object> redisTemplate;

    @Resource
    TestController testController;
    @Test
    public void test(){
//        System.out.println(testController.get());
    }
}
