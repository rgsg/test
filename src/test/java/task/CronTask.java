package task;

import java.net.URL;
import java.util.Enumeration;

/**
 * @author cai ping
 * @description:
 * @date 2024-02-01
 */
public class CronTask {
    public static void main(String[] args) throws Exception {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        Enumeration<URL> resources = classLoader.getResources("META-INF/spring.factories");
        while (resources.hasMoreElements()){
            URL url = resources.nextElement();
            System.out.println(url);
        }
        System.out.println();
    }

}