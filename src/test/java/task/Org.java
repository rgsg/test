package task;

import lombok.Data;

import java.nio.charset.StandardCharsets;

@Data
public class Org {
    private int id;

    private String name;

    private int parentId;

    private int age;

    public static void main(String[] args) {
        byte[] b = "[68".getBytes(StandardCharsets.UTF_8);
        System.out.println(b);
    }

}