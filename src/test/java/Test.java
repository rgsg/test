import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author cai ping
 * @description:
 * @date 2024-09-25
 */
public class Test<T extends Number,X>  {

    public List<? extends Number> list;

    public void setList(List<? extends Number> list) {
        this.list = list;
    }
    //    public String name;



    public static <K, V, T> Map<K, V> toMap(List<T> data, Function<T, K> kFunc, Function<T, V> vFunc) {

        return data.stream().collect(Collectors.toMap(kFunc,vFunc));
    }

    public static void main(String[] args) {

        Field[] declaredFields = Test.class.getDeclaredFields();


        /**
         * Type 类型描述，所有类的父接口
         * ParameterizedType 参数化类型 即泛型定义List<T>
         * TypeVariable 泛型中的变量 T
         * GenericArrayType 泛型数组类型
         * WildcardType 泛型表达式
         */
        for (Field field : declaredFields) {
            // 获取泛型类型
            Type genericType = field.getGenericType();
            //如果类型定义是一种参数化类型，则返回ParameterizedType的实现描述，描述源代码中的类型描述

            if(genericType instanceof ParameterizedType) {
                ParameterizedType genericType1 = (ParameterizedType) genericType;
                // 获取真实的泛型类型,TypeVariable
                Type[] actualTypeArguments = genericType1.getActualTypeArguments();
                // typeVariable类型

                for (Type type : actualTypeArguments) {
//                    TypeVariable typeVariable = (TypeVariable) type;
//                    System.out.println(typeVariable.getName());
//                    System.out.println(Arrays.toString(typeVariable.getBounds()));
//                    Type[] bounds = typeVariable.getBounds();
                       if(type instanceof WildcardType) {
                           WildcardType w = (WildcardType) type;
                           System.out.println(Arrays.toString(w.getLowerBounds()));
                       }
                }
                System.out.println("genericType1.getActualTypeArguments() = " + Arrays.toString(genericType1.getActualTypeArguments()));
            }else {
                System.out.println("-----" + field.getName());
            }
        }
    }
}
