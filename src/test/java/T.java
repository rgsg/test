/**
 * @author cai ping
 * @description:
 * @date 2024-08-23
 */
public class T {
    public static void main(String[] args) {
//        ArrayList<T extends TestOne> objects = new ArrayList<>();

        // TCC XA
        // XA需要数据库对事务支持
        // 全局锁，强一致
        // 业务代码控制
        // Try Commit cancel

        // 业务系统中一个特定的业务逻辑，在其对外提供服务时，必须接受不确定性，即主业务的初步操作都是临时性的操作，
        // 在业务处理失败时应该保留取消权，当参与事务发生异常时，所有的事务参与者应当取消之前的操作
        // 当主业务认为事务应该提交时，事务的所有参与者，应该提交相应的操作。每次初步操作，最终都会对应着提交或者取消

        // Try 资源预处理，临时性操作，为提交提供资源信息
        // Commit 提交请求，当try步骤执行成功后调用
        // cancel 取消操作，释放try的资源

        // TCC的三个问题，空回滚，幂等，悬挂
        // 空回滚，当try阶段执行处理失败

    }
}
