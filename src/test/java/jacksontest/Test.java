package jacksontest;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

/**
 * @author cai ping
 * @description:
 * @date 2024-08-19
 */
public class Test {

    /**
     * feign调用，参数类型可以不一致，但是反序列化的类型属性需要覆盖序列化类型的属性
     * @param args
     */
    @SneakyThrows
    public static void main(String[] args) {

        Person person = new Person();
        person.setSex("男");
        person.setName("zhangsan");
        person.setAge("20");

        ObjectMapper objectMapper = new ObjectMapper();


        String s = objectMapper.writeValueAsString(person);

        Student student = objectMapper.readValue(s, Student.class);
        System.out.println(student);
    }
}
