package jacksontest;

import lombok.Data;

/**
 * @author cai ping
 * @description:
 * @date 2024-08-19
 */
@Data
public class Person {

    private String name;

    private String age;

    private String sex;

}
