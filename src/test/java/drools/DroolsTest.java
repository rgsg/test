package drools;

import org.example.Main;
import org.example.drools.Order;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author cai ping
 * @description:
 * @date 2024-12-11
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Main.class)
public class DroolsTest {
    @Resource
    private KieContainer kieContainer;
    @Test
    public void test01() {

        KieSession kieSession = kieContainer.newKieSession();

        Order order = new Order();
        order.setAmout(100);

        kieSession.insert(order);
        kieSession.fireAllRules();
        kieSession.dispose();

        System.out.println("order.getScore() = " + order.getScore());
        System.out.println("order.getAmount() = " + order.getAmount());
    }

}
