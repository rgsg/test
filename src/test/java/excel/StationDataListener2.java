package excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author cai ping
 * @description:
 * @date 2024-01-19
 */
public class StationDataListener2 extends AnalysisEventListener<Map<Integer, String>> {

    /**
     * 行号
     */
    private int rowNum = 0;

    /**
     * 站点数量
     */
    private int stationNum = 0;

    /**
     * 一个站点占用的列数
     */
    private int stationSize = 2;

    private List<String> header = Lists.newArrayList();

    private Map<String, Map<String, Double>> data = Maps.newHashMap();


    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {

        header = headMap.values().stream().filter(head -> !Objects.isNull(head)).collect(Collectors.toList());
        stationNum = header.size();

    }

    @Override
    public void invoke(Map<Integer, String> excelData, AnalysisContext context) {
        excelData.remove(0);
        if (rowNum == 0) {
            rowNum++;
            return;
        }
        for (int i = 0; i < excelData.size() / stationSize; i++) {
            String stationCode = header.get(i);
            Map<String, Double> stringMapMap = data.getOrDefault(stationCode,new HashMap<>());

            String time = formatTime(excelData.get(1));
            String value = excelData.get(stationSize * i + 2);
            if(!Objects.isNull(value)){
                double v1 = Double.parseDouble(value);
                stringMapMap.put(time, v1);
                stringMapMap.put(formatTime2(excelData.get(1)), Double.sum(v1,Double.parseDouble(excelData.get(stationSize * i + 3))));
                data.put(stationCode,stringMapMap);
            }
        }
    }
    public String formatTime(String dateTime) {
        Date date = new Date(dateTime);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        String time = simpleDateFormat.format(date);
        return time;
    }
    public String formatTime2(String dateTime) {
        Date date = new Date(dateTime);
        date.setHours(23);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        String time = simpleDateFormat.format(date);
        return time;
    }
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

        data.forEach((key,value) ->{
            ArrayList<UploadData> objects = new ArrayList<>();

           value.forEach((time,data) ->{
               UploadData uploadData = new UploadData();
               uploadData.setDeviceCode(key);
               uploadData.setAcquisitionTime(time);
               HashMap<Object, Object> hashMap = new HashMap<>();
               hashMap.put("LJLL_CS",data);
               uploadData.setData(hashMap);
               objects.add(uploadData);
           });
            List<UploadData> collect = objects.stream().sorted(Comparator.comparing(UploadData::getAcquisitionTime)).collect(Collectors.toList());
            System.out.println(JSONObject.toJSONString(collect));
        });
    }
}
