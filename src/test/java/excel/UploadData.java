package excel;

import lombok.Data;

@Data
public class UploadData {
    private String acquisitionTime;
    private Object data;
    private String deviceCode;

}