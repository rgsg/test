package excel;

import lombok.Data;
import lombok.ToString;

/**
 * @author cai ping
 * @description:
 * @date 2024-01-19
 */

@Data
@ToString
public class StationData {

    private String time;

    private String SSLL_CS;



}
