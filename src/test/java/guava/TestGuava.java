package guava;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.SneakyThrows;

/**
 * @author cai ping
 * @description:
 * @date 2024-10-21
 */
public class TestGuava {
    @SneakyThrows
    public static void main(String[] args) {
        CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder();
        Cache<Object, Object> cache = cacheBuilder
                .initialCapacity(1)
                .maximumSize(1)
                .build();

        cache.put("name", "lisi");
        Object o = cache.get("name", () -> "");

        System.out.println(o);

        cache.put("email", "lisi@qq.com");

        System.out.println(cache.get("name", () -> ""));


    }
}
